import React from 'react';
import {
  act,
  render,
  wait,
  findByTestId,
} from '@testing-library/react';
import { MockedProvider } from '@apollo/react-testing';
import { Home, GET_COUNTRIES } from './home.page';

const mocks = [
  {
    request: {
      query: GET_COUNTRIES,
    },
    result: {
      data: {
        Country: [
          {
            name: 'Afghanistan',
            area: 652230,
            capital: 'Kabul',
            population: 27657145,
            populationDensity: 42.40397559143247,
            flag: {
              svgFile: 'https://restcountries.eu/data/afg.svg',
            },
            topLevelDomains: [
              {
                name: '.af',
              },
            ],
          },
          {
            name: 'Åland Islands',
            area: 1580,
            capital: 'Mariehamn',
            population: 28875,
            populationDensity: 18.275316455696203,
            flag: {
              svgFile: 'https://restcountries.eu/data/ala.svg',
            },
            topLevelDomains: [
              {
                name: '.ax',
              },
            ],
          },
          {
            name: 'Albania',
            area: 28748,
            capital: 'Tirana',
            population: 2886026,
            populationDensity: 100.39049673020732,
            flag: {
              svgFile: 'https://restcountries.eu/data/alb.svg',
            },
            topLevelDomains: [
              {
                name: '.al',
              },
            ],
          },
          {
            name: 'Algeria',
            area: 2381741,
            capital: 'Algiers',
            population: 40400000,
            populationDensity: 16.962381719926725,
            flag: {
              svgFile: 'https://restcountries.eu/data/dza.svg',
            },
            topLevelDomains: [
              {
                name: '.dz',
              },
            ],
          },
          {
            name: 'American Samoa',
            area: 199,
            capital: 'Pago Pago',
            population: 57100,
            populationDensity: 286.93467336683415,
            flag: {
              svgFile: 'https://restcountries.eu/data/asm.svg',
            },
            topLevelDomains: [
              {
                name: '.as',
              },
            ],
          },
          {
            name: 'Andorra',
            area: 468,
            capital: 'Andorra la Vella',
            population: 78014,
            populationDensity: 166.6965811965812,
            flag: {
              svgFile: 'https://restcountries.eu/data/and.svg',
            },
            topLevelDomains: [
              {
                name: '.ad',
              },
            ],
          },
          {
            name: 'Angola',
            area: 1246700,
            capital: 'Luanda',
            population: 25868000,
            populationDensity: 20.7491778294698,
            flag: {
              svgFile: 'https://restcountries.eu/data/ago.svg',
            },
            topLevelDomains: [
              {
                name: '.ao',
              },
            ],
          },
          {
            name: 'Anguilla',
            area: 91,
            capital: 'The Valley',
            population: 13452,
            populationDensity: 147.82417582417582,
            flag: {
              svgFile: 'https://restcountries.eu/data/aia.svg',
            },
            topLevelDomains: [
              {
                name: '.ai',
              },
            ],
          },
          {
            name: 'Antarctica',
            area: 14000000,
            capital: '',
            population: 1000,
            populationDensity: 0.00007142857142857143,
            flag: {
              svgFile: 'https://restcountries.eu/data/ata.svg',
            },
            topLevelDomains: [
              {
                name: '.aq',
              },
            ],
          },
          {
            name: 'Antigua and Barbuda',
            area: 442,
            capital: "Saint John's",
            population: 86295,
            populationDensity: 195.23755656108597,
            flag: {
              svgFile: 'https://restcountries.eu/data/atg.svg',
            },
            topLevelDomains: [
              {
                name: '.ag',
              },
            ],
          },
          {
            name: 'Argentina',
            area: 2780400,
            capital: 'Buenos Aires',
            population: 43590400,
            populationDensity: 15.677744209466264,
            flag: {
              svgFile: 'https://restcountries.eu/data/arg.svg',
            },
            topLevelDomains: [
              {
                name: '.ar',
              },
            ],
          },
          {
            name: 'Armenia',
            area: 29743,
            capital: 'Yerevan',
            population: 2994400,
            populationDensity: 100.6757892613388,
            flag: {
              svgFile: 'https://restcountries.eu/data/arm.svg',
            },
            topLevelDomains: [
              {
                name: '.am',
              },
            ],
          },
          {
            name: 'Aruba',
            area: 180,
            capital: 'Oranjestad',
            population: 107394,
            populationDensity: 596.6333333333333,
            flag: {
              svgFile: 'https://restcountries.eu/data/abw.svg',
            },
            topLevelDomains: [
              {
                name: '.aw',
              },
            ],
          },
          {
            name: 'Australia',
            area: 7692024,
            capital: 'Canberra',
            population: 24117360,
            populationDensity: 3.135372432535312,
            flag: {
              svgFile: 'https://restcountries.eu/data/aus.svg',
            },
            topLevelDomains: [
              {
                name: '.au',
              },
            ],
          },
          {
            name: 'Austria',
            area: 83871,
            capital: 'Vienna',
            population: 8725931,
            populationDensity: 104.03990652311288,
            flag: {
              svgFile: 'https://restcountries.eu/data/aut.svg',
            },
            topLevelDomains: [
              {
                name: '.at',
              },
            ],
          },
          {
            name: 'Azerbaijan',
            area: 86600,
            capital: 'Baku',
            population: 9730500,
            populationDensity: 112.36143187066975,
            flag: {
              svgFile: 'https://restcountries.eu/data/aze.svg',
            },
            topLevelDomains: [
              {
                name: '.az',
              },
            ],
          },
          {
            name: 'Bahamas',
            area: 13943,
            capital: 'Nassau',
            population: 378040,
            populationDensity: 27.113246790504196,
            flag: {
              svgFile: 'https://restcountries.eu/data/bhs.svg',
            },
            topLevelDomains: [
              {
                name: '.bs',
              },
            ],
          },
          {
            name: 'Bahrain',
            area: 765,
            capital: 'Manama',
            population: 1404900,
            populationDensity: 1836.4705882352941,
            flag: {
              svgFile: 'https://restcountries.eu/data/bhr.svg',
            },
            topLevelDomains: [
              {
                name: '.bh',
              },
            ],
          },
          {
            name: 'Bangladesh',
            area: 147570,
            capital: 'Dhaka',
            population: 161006790,
            populationDensity: 1091.053669445009,
            flag: {
              svgFile: 'https://restcountries.eu/data/bgd.svg',
            },
            topLevelDomains: [
              {
                name: '.bd',
              },
            ],
          },
          {
            name: 'Barbados',
            area: 430,
            capital: 'Bridgetown',
            population: 285000,
            populationDensity: 662.7906976744187,
            flag: {
              svgFile: 'https://restcountries.eu/data/brb.svg',
            },
            topLevelDomains: [
              {
                name: '.bb',
              },
            ],
          },
          {
            name: 'Belarus',
            area: 207600,
            capital: 'Minsk',
            population: 9498700,
            populationDensity: 45.75481695568401,
            flag: {
              svgFile: 'https://restcountries.eu/data/blr.svg',
            },
            topLevelDomains: [
              {
                name: '.by',
              },
            ],
          },
          {
            name: 'Belgium',
            area: 30528,
            capital: 'Brussels',
            population: 11319511,
            populationDensity: 370.79110980083857,
            flag: {
              svgFile: 'https://restcountries.eu/data/bel.svg',
            },
            topLevelDomains: [
              {
                name: '.be',
              },
            ],
          },
          {
            name: 'Belize',
            area: 22966,
            capital: 'Belmopan',
            population: 370300,
            populationDensity: 16.123835234694766,
            flag: {
              svgFile: 'https://restcountries.eu/data/blz.svg',
            },
            topLevelDomains: [
              {
                name: '.bz',
              },
            ],
          },
          {
            name: 'Benin',
            area: 112622,
            capital: 'Porto-Novo',
            population: 10653654,
            populationDensity: 94.5965619505958,
            flag: {
              svgFile: 'https://restcountries.eu/data/ben.svg',
            },
            topLevelDomains: [
              {
                name: '.bj',
              },
            ],
          },
          {
            name: 'Bermuda',
            area: 54,
            capital: 'Hamilton',
            population: 61954,
            populationDensity: 1147.2962962962963,
            flag: {
              svgFile: 'https://restcountries.eu/data/bmu.svg',
            },
            topLevelDomains: [
              {
                name: '.bm',
              },
            ],
          },
          {
            name: 'Bhutan',
            area: 38394,
            capital: 'Thimphu',
            population: 775620,
            populationDensity: 20.201593999062354,
            flag: {
              svgFile: 'https://restcountries.eu/data/btn.svg',
            },
            topLevelDomains: [
              {
                name: '.bt',
              },
            ],
          },
          {
            name: 'Bolivia (Plurinational State of)',
            area: 1098581,
            capital: 'Sucre',
            population: 10985059,
            populationDensity: 9.999316390871497,
            flag: {
              svgFile: 'https://restcountries.eu/data/bol.svg',
            },
            topLevelDomains: [
              {
                name: '.bo',
              },
            ],
          },
          {
            name: 'Bonaire, Sint Eustatius and Saba',
            area: 294,
            capital: 'Kralendijk',
            population: 17408,
            populationDensity: 59.2108843537415,
            flag: {
              svgFile: 'https://restcountries.eu/data/bes.svg',
            },
            topLevelDomains: [
              {
                name: '.nl',
              },
              {
                name: '.an',
              },
            ],
          },
          {
            name: 'Bosnia and Herzegovina',
            area: 51209,
            capital: 'Sarajevo',
            population: 3531159,
            populationDensity: 68.95582807709582,
            flag: {
              svgFile: 'https://restcountries.eu/data/bih.svg',
            },
            topLevelDomains: [
              {
                name: '.ba',
              },
            ],
          },
          {
            name: 'Botswana',
            area: 582000,
            capital: 'Gaborone',
            population: 2141206,
            populationDensity: 3.6790481099656356,
            flag: {
              svgFile: 'https://restcountries.eu/data/bwa.svg',
            },
            topLevelDomains: [
              {
                name: '.bw',
              },
            ],
          },
          {
            name: 'Bouvet Island',
            area: 49,
            capital: '',
            population: 0,
            populationDensity: 0,
            flag: {
              svgFile: 'https://restcountries.eu/data/bvt.svg',
            },
            topLevelDomains: [
              {
                name: '.bv',
              },
            ],
          },
          {
            name: 'Brazil',
            area: 8515767,
            capital: 'Brasília',
            population: 206135893,
            populationDensity: 24.20638011819722,
            flag: {
              svgFile: 'https://restcountries.eu/data/bra.svg',
            },
            topLevelDomains: [
              {
                name: '.br',
              },
            ],
          },
          {
            name: 'British Indian Ocean Territory',
            area: 60,
            capital: 'Diego Garcia',
            population: 3000,
            populationDensity: 50,
            flag: {
              svgFile: 'https://restcountries.eu/data/iot.svg',
            },
            topLevelDomains: [
              {
                name: '.io',
              },
            ],
          },
          {
            name: 'United States Minor Outlying Islands',
            area: null,
            capital: '',
            population: 300,
            populationDensity: null,
            flag: {
              svgFile: 'https://restcountries.eu/data/umi.svg',
            },
            topLevelDomains: [
              {
                name: '.us',
              },
            ],
          },
          {
            name: 'Virgin Islands (British)',
            area: 151,
            capital: 'Road Town',
            population: 28514,
            populationDensity: 188.83443708609272,
            flag: {
              svgFile: 'https://restcountries.eu/data/vgb.svg',
            },
            topLevelDomains: [
              {
                name: '.vg',
              },
            ],
          },
          {
            name: 'Virgin Islands (U.S.)',
            area: 346.36,
            capital: 'Charlotte Amalie',
            population: 114743,
            populationDensity: 331.2824806559649,
            flag: {
              svgFile: 'https://restcountries.eu/data/vir.svg',
            },
            topLevelDomains: [
              {
                name: '.vi',
              },
            ],
          },
          {
            name: 'Brunei Darussalam',
            area: 5765,
            capital: 'Bandar Seri Begawan',
            population: 411900,
            populationDensity: 71.44839549002602,
            flag: {
              svgFile: 'https://restcountries.eu/data/brn.svg',
            },
            topLevelDomains: [
              {
                name: '.bn',
              },
            ],
          },
          {
            name: 'Bulgaria',
            area: 110879,
            capital: 'Sofia',
            population: 7153784,
            populationDensity: 64.51883584808665,
            flag: {
              svgFile: 'https://restcountries.eu/data/bgr.svg',
            },
            topLevelDomains: [
              {
                name: '.bg',
              },
            ],
          },
          {
            name: 'Burkina Faso',
            area: 272967,
            capital: 'Ouagadougou',
            population: 19034397,
            populationDensity: 69.73149501588104,
            flag: {
              svgFile: 'https://restcountries.eu/data/bfa.svg',
            },
            topLevelDomains: [
              {
                name: '.bf',
              },
            ],
          },
          {
            name: 'Burundi',
            area: 27834,
            capital: 'Bujumbura',
            population: 10114505,
            populationDensity: 363.3866853488539,
            flag: {
              svgFile: 'https://restcountries.eu/data/bdi.svg',
            },
            topLevelDomains: [
              {
                name: '.bi',
              },
            ],
          },
          {
            name: 'Cambodia',
            area: 181035,
            capital: 'Phnom Penh',
            population: 15626444,
            populationDensity: 86.3172535697517,
            flag: {
              svgFile: 'https://restcountries.eu/data/khm.svg',
            },
            topLevelDomains: [
              {
                name: '.kh',
              },
            ],
          },
          {
            name: 'Cameroon',
            area: 475442,
            capital: 'Yaoundé',
            population: 22709892,
            populationDensity: 47.765851565490635,
            flag: {
              svgFile: 'https://restcountries.eu/data/cmr.svg',
            },
            topLevelDomains: [
              {
                name: '.cm',
              },
            ],
          },
          {
            name: 'Canada',
            area: 9984670,
            capital: 'Ottawa',
            population: 36155487,
            populationDensity: 3.6210998460640162,
            flag: {
              svgFile: 'https://restcountries.eu/data/can.svg',
            },
            topLevelDomains: [
              {
                name: '.ca',
              },
            ],
          },
          {
            name: 'Cabo Verde',
            area: 4033,
            capital: 'Praia',
            population: 531239,
            populationDensity: 131.72303496156707,
            flag: {
              svgFile: 'https://restcountries.eu/data/cpv.svg',
            },
            topLevelDomains: [
              {
                name: '.cv',
              },
            ],
          },
          {
            name: 'Cayman Islands',
            area: 264,
            capital: 'George Town',
            population: 58238,
            populationDensity: 220.59848484848484,
            flag: {
              svgFile: 'https://restcountries.eu/data/cym.svg',
            },
            topLevelDomains: [
              {
                name: '.ky',
              },
            ],
          },
          {
            name: 'Central African Republic',
            area: 622984,
            capital: 'Bangui',
            population: 4998000,
            populationDensity: 8.022677949995506,
            flag: {
              svgFile: 'https://restcountries.eu/data/caf.svg',
            },
            topLevelDomains: [
              {
                name: '.cf',
              },
            ],
          },
          {
            name: 'Chad',
            area: 1284000,
            capital: "N'Djamena",
            population: 14497000,
            populationDensity: 11.290498442367602,
            flag: {
              svgFile: 'https://restcountries.eu/data/tcd.svg',
            },
            topLevelDomains: [
              {
                name: '.td',
              },
            ],
          },
          {
            name: 'Chile',
            area: 756102,
            capital: 'Santiago',
            population: 18191900,
            populationDensity: 24.060113582558966,
            flag: {
              svgFile: 'https://restcountries.eu/data/chl.svg',
            },
            topLevelDomains: [
              {
                name: '.cl',
              },
            ],
          },
          {
            name: 'China',
            area: 9640011,
            capital: 'Beijing',
            population: 1377422166,
            populationDensity: 142.88595376084115,
            flag: {
              svgFile: 'https://restcountries.eu/data/chn.svg',
            },
            topLevelDomains: [
              {
                name: '.cn',
              },
            ],
          },
          {
            name: 'Christmas Island',
            area: 135,
            capital: 'Flying Fish Cove',
            population: 2072,
            populationDensity: 15.348148148148148,
            flag: {
              svgFile: 'https://restcountries.eu/data/cxr.svg',
            },
            topLevelDomains: [
              {
                name: '.cx',
              },
            ],
          },
          {
            name: 'Cocos (Keeling) Islands',
            area: 14,
            capital: 'West Island',
            population: 550,
            populationDensity: 39.285714285714285,
            flag: {
              svgFile: 'https://restcountries.eu/data/cck.svg',
            },
            topLevelDomains: [
              {
                name: '.cc',
              },
            ],
          },
          {
            name: 'Colombia',
            area: 1141748,
            capital: 'Bogotá',
            population: 48759958,
            populationDensity: 42.70640982073102,
            flag: {
              svgFile: 'https://restcountries.eu/data/col.svg',
            },
            topLevelDomains: [
              {
                name: '.co',
              },
            ],
          },
          {
            name: 'Comoros',
            area: 1862,
            capital: 'Moroni',
            population: 806153,
            populationDensity: 432.9500537056928,
            flag: {
              svgFile: 'https://restcountries.eu/data/com.svg',
            },
            topLevelDomains: [
              {
                name: '.km',
              },
            ],
          },
          {
            name: 'Congo',
            area: 342000,
            capital: 'Brazzaville',
            population: 4741000,
            populationDensity: 13.862573099415204,
            flag: {
              svgFile: 'https://restcountries.eu/data/cog.svg',
            },
            topLevelDomains: [
              {
                name: '.cg',
              },
            ],
          },
          {
            name: 'Congo (Democratic Republic of the)',
            area: 2344858,
            capital: 'Kinshasa',
            population: 85026000,
            populationDensity: 36.26061791375,
            flag: {
              svgFile: 'https://restcountries.eu/data/cod.svg',
            },
            topLevelDomains: [
              {
                name: '.cd',
              },
            ],
          },
          {
            name: 'Cook Islands',
            area: 236,
            capital: 'Avarua',
            population: 18100,
            populationDensity: 76.69491525423729,
            flag: {
              svgFile: 'https://restcountries.eu/data/cok.svg',
            },
            topLevelDomains: [
              {
                name: '.ck',
              },
            ],
          },
          {
            name: 'Costa Rica',
            area: 51100,
            capital: 'San José',
            population: 4890379,
            populationDensity: 95.70213307240705,
            flag: {
              svgFile: 'https://restcountries.eu/data/cri.svg',
            },
            topLevelDomains: [
              {
                name: '.cr',
              },
            ],
          },
          {
            name: 'Croatia',
            area: 56594,
            capital: 'Zagreb',
            population: 4190669,
            populationDensity: 74.04793794395165,
            flag: {
              svgFile: 'https://restcountries.eu/data/hrv.svg',
            },
            topLevelDomains: [
              {
                name: '.hr',
              },
            ],
          },
          {
            name: 'Cuba',
            area: 109884,
            capital: 'Havana',
            population: 11239004,
            populationDensity: 102.28062320265006,
            flag: {
              svgFile: 'https://restcountries.eu/data/cub.svg',
            },
            topLevelDomains: [
              {
                name: '.cu',
              },
            ],
          },
          {
            name: 'Curaçao',
            area: 444,
            capital: 'Willemstad',
            population: 154843,
            populationDensity: 348.7454954954955,
            flag: {
              svgFile: 'https://restcountries.eu/data/cuw.svg',
            },
            topLevelDomains: [
              {
                name: '.cw',
              },
            ],
          },
          {
            name: 'Cyprus',
            area: 9251,
            capital: 'Nicosia',
            population: 847000,
            populationDensity: 91.5576694411415,
            flag: {
              svgFile: 'https://restcountries.eu/data/cyp.svg',
            },
            topLevelDomains: [
              {
                name: '.cy',
              },
            ],
          },
          {
            name: 'Czech Republic',
            area: 78865,
            capital: 'Prague',
            population: 10558524,
            populationDensity: 133.88098649591075,
            flag: {
              svgFile: 'https://restcountries.eu/data/cze.svg',
            },
            topLevelDomains: [
              {
                name: '.cz',
              },
            ],
          },
          {
            name: 'Denmark',
            area: 43094,
            capital: 'Copenhagen',
            population: 5717014,
            populationDensity: 132.66380470599157,
            flag: {
              svgFile: 'https://restcountries.eu/data/dnk.svg',
            },
            topLevelDomains: [
              {
                name: '.dk',
              },
            ],
          },
          {
            name: 'Djibouti',
            area: 23200,
            capital: 'Djibouti',
            population: 900000,
            populationDensity: 38.793103448275865,
            flag: {
              svgFile: 'https://restcountries.eu/data/dji.svg',
            },
            topLevelDomains: [
              {
                name: '.dj',
              },
            ],
          },
          {
            name: 'Dominica',
            area: 751,
            capital: 'Roseau',
            population: 71293,
            populationDensity: 94.93075898801598,
            flag: {
              svgFile: 'https://restcountries.eu/data/dma.svg',
            },
            topLevelDomains: [
              {
                name: '.dm',
              },
            ],
          },
          {
            name: 'Dominican Republic',
            area: 48671,
            capital: 'Santo Domingo',
            population: 10075045,
            populationDensity: 207.00304082513202,
            flag: {
              svgFile: 'https://restcountries.eu/data/dom.svg',
            },
            topLevelDomains: [
              {
                name: '.do',
              },
            ],
          },
          {
            name: 'Ecuador',
            area: 276841,
            capital: 'Quito',
            population: 16545799,
            populationDensity: 59.766432717697164,
            flag: {
              svgFile: 'https://restcountries.eu/data/ecu.svg',
            },
            topLevelDomains: [
              {
                name: '.ec',
              },
            ],
          },
          {
            name: 'Egypt',
            area: 1002450,
            capital: 'Cairo',
            population: 91290000,
            populationDensity: 91.066886128984,
            flag: {
              svgFile: 'https://restcountries.eu/data/egy.svg',
            },
            topLevelDomains: [
              {
                name: '.eg',
              },
            ],
          },
          {
            name: 'El Salvador',
            area: 21041,
            capital: 'San Salvador',
            population: 6520675,
            populationDensity: 309.9032840644456,
            flag: {
              svgFile: 'https://restcountries.eu/data/slv.svg',
            },
            topLevelDomains: [
              {
                name: '.sv',
              },
            ],
          },
          {
            name: 'Equatorial Guinea',
            area: 28051,
            capital: 'Malabo',
            population: 1222442,
            populationDensity: 43.579266336315996,
            flag: {
              svgFile: 'https://restcountries.eu/data/gnq.svg',
            },
            topLevelDomains: [
              {
                name: '.gq',
              },
            ],
          },
          {
            name: 'Eritrea',
            area: 117600,
            capital: 'Asmara',
            population: 5352000,
            populationDensity: 45.51020408163265,
            flag: {
              svgFile: 'https://restcountries.eu/data/eri.svg',
            },
            topLevelDomains: [
              {
                name: '.er',
              },
            ],
          },
          {
            name: 'Estonia',
            area: 45227,
            capital: 'Tallinn',
            population: 1315944,
            populationDensity: 29.096424702058506,
            flag: {
              svgFile: 'https://restcountries.eu/data/est.svg',
            },
            topLevelDomains: [
              {
                name: '.ee',
              },
            ],
          },
          {
            name: 'Ethiopia',
            area: 1104300,
            capital: 'Addis Ababa',
            population: 92206005,
            populationDensity: 83.49724259712035,
            flag: {
              svgFile: 'https://restcountries.eu/data/eth.svg',
            },
            topLevelDomains: [
              {
                name: '.et',
              },
            ],
          },
          {
            name: 'Falkland Islands (Malvinas)',
            area: 12173,
            capital: 'Stanley',
            population: 2563,
            populationDensity: 0.21054793395218926,
            flag: {
              svgFile: 'https://restcountries.eu/data/flk.svg',
            },
            topLevelDomains: [
              {
                name: '.fk',
              },
            ],
          },
          {
            name: 'Faroe Islands',
            area: 1393,
            capital: 'Tórshavn',
            population: 49376,
            populationDensity: 35.44580043072505,
            flag: {
              svgFile: 'https://restcountries.eu/data/fro.svg',
            },
            topLevelDomains: [
              {
                name: '.fo',
              },
            ],
          },
          {
            name: 'Fiji',
            area: 18272,
            capital: 'Suva',
            population: 867000,
            populationDensity: 47.44964973730298,
            flag: {
              svgFile: 'https://restcountries.eu/data/fji.svg',
            },
            topLevelDomains: [
              {
                name: '.fj',
              },
            ],
          },
          {
            name: 'Finland',
            area: 338424,
            capital: 'Helsinki',
            population: 5491817,
            populationDensity: 16.227622745431766,
            flag: {
              svgFile: 'https://restcountries.eu/data/fin.svg',
            },
            topLevelDomains: [
              {
                name: '.fi',
              },
            ],
          },
          {
            name: 'France',
            area: 640679,
            capital: 'Paris',
            population: 66710000,
            populationDensity: 104.12390604343204,
            flag: {
              svgFile: 'https://restcountries.eu/data/fra.svg',
            },
            topLevelDomains: [
              {
                name: '.fr',
              },
            ],
          },
          {
            name: 'French Guiana',
            area: null,
            capital: 'Cayenne',
            population: 254541,
            populationDensity: null,
            flag: {
              svgFile: 'https://restcountries.eu/data/guf.svg',
            },
            topLevelDomains: [
              {
                name: '.gf',
              },
            ],
          },
          {
            name: 'French Polynesia',
            area: 4167,
            capital: 'Papeetē',
            population: 271800,
            populationDensity: 65.2267818574514,
            flag: {
              svgFile: 'https://restcountries.eu/data/pyf.svg',
            },
            topLevelDomains: [
              {
                name: '.pf',
              },
            ],
          },
          {
            name: 'French Southern Territories',
            area: 7747,
            capital: 'Port-aux-Français',
            population: 140,
            populationDensity: 0.01807151155285917,
            flag: {
              svgFile: 'https://restcountries.eu/data/atf.svg',
            },
            topLevelDomains: [
              {
                name: '.tf',
              },
            ],
          },
          {
            name: 'Gabon',
            area: 267668,
            capital: 'Libreville',
            population: 1802278,
            populationDensity: 6.733259112034311,
            flag: {
              svgFile: 'https://restcountries.eu/data/gab.svg',
            },
            topLevelDomains: [
              {
                name: '.ga',
              },
            ],
          },
          {
            name: 'Gambia',
            area: 11295,
            capital: 'Banjul',
            population: 1882450,
            populationDensity: 166.6622399291722,
            flag: {
              svgFile: 'https://restcountries.eu/data/gmb.svg',
            },
            topLevelDomains: [
              {
                name: '.gm',
              },
            ],
          },
          {
            name: 'Georgia',
            area: 69700,
            capital: 'Tbilisi',
            population: 3720400,
            populationDensity: 53.377331420373025,
            flag: {
              svgFile: 'https://restcountries.eu/data/geo.svg',
            },
            topLevelDomains: [
              {
                name: '.ge',
              },
            ],
          },
          {
            name: 'Germany',
            area: 357114,
            capital: 'Berlin',
            population: 81770900,
            populationDensity: 228.97702134332454,
            flag: {
              svgFile: 'https://restcountries.eu/data/deu.svg',
            },
            topLevelDomains: [
              {
                name: '.de',
              },
            ],
          },
          {
            name: 'Ghana',
            area: 238533,
            capital: 'Accra',
            population: 27670174,
            populationDensity: 116.00145053304993,
            flag: {
              svgFile: 'https://restcountries.eu/data/gha.svg',
            },
            topLevelDomains: [
              {
                name: '.gh',
              },
            ],
          },
          {
            name: 'Gibraltar',
            area: 6,
            capital: 'Gibraltar',
            population: 33140,
            populationDensity: 5523.333333333333,
            flag: {
              svgFile: 'https://restcountries.eu/data/gib.svg',
            },
            topLevelDomains: [
              {
                name: '.gi',
              },
            ],
          },
          {
            name: 'Greece',
            area: 131990,
            capital: 'Athens',
            population: 10858018,
            populationDensity: 82.26394423819987,
            flag: {
              svgFile: 'https://restcountries.eu/data/grc.svg',
            },
            topLevelDomains: [
              {
                name: '.gr',
              },
            ],
          },
          {
            name: 'Greenland',
            area: 2166086,
            capital: 'Nuuk',
            population: 55847,
            populationDensity: 0.02578244815764471,
            flag: {
              svgFile: 'https://restcountries.eu/data/grl.svg',
            },
            topLevelDomains: [
              {
                name: '.gl',
              },
            ],
          },
          {
            name: 'Grenada',
            area: 344,
            capital: "St. George's",
            population: 103328,
            populationDensity: 300.3720930232558,
            flag: {
              svgFile: 'https://restcountries.eu/data/grd.svg',
            },
            topLevelDomains: [
              {
                name: '.gd',
              },
            ],
          },
          {
            name: 'Guadeloupe',
            area: null,
            capital: 'Basse-Terre',
            population: 400132,
            populationDensity: null,
            flag: {
              svgFile: 'https://restcountries.eu/data/glp.svg',
            },
            topLevelDomains: [
              {
                name: '.gp',
              },
            ],
          },
          {
            name: 'Guam',
            area: 549,
            capital: 'Hagåtña',
            population: 184200,
            populationDensity: 335.5191256830601,
            flag: {
              svgFile: 'https://restcountries.eu/data/gum.svg',
            },
            topLevelDomains: [
              {
                name: '.gu',
              },
            ],
          },
          {
            name: 'Guatemala',
            area: 108889,
            capital: 'Guatemala City',
            population: 16176133,
            populationDensity: 148.55617188145726,
            flag: {
              svgFile: 'https://restcountries.eu/data/gtm.svg',
            },
            topLevelDomains: [
              {
                name: '.gt',
              },
            ],
          },
          {
            name: 'Guernsey',
            area: 78,
            capital: 'St. Peter Port',
            population: 62999,
            populationDensity: 807.6794871794872,
            flag: {
              svgFile: 'https://restcountries.eu/data/ggy.svg',
            },
            topLevelDomains: [
              {
                name: '.gg',
              },
            ],
          },
          {
            name: 'Guinea',
            area: 245857,
            capital: 'Conakry',
            population: 12947000,
            populationDensity: 52.660693004470076,
            flag: {
              svgFile: 'https://restcountries.eu/data/gin.svg',
            },
            topLevelDomains: [
              {
                name: '.gn',
              },
            ],
          },
          {
            name: 'Guinea-Bissau',
            area: 36125,
            capital: 'Bissau',
            population: 1547777,
            populationDensity: 42.845038062283734,
            flag: {
              svgFile: 'https://restcountries.eu/data/gnb.svg',
            },
            topLevelDomains: [
              {
                name: '.gw',
              },
            ],
          },
          {
            name: 'Guyana',
            area: 214969,
            capital: 'Georgetown',
            population: 746900,
            populationDensity: 3.4744544562239206,
            flag: {
              svgFile: 'https://restcountries.eu/data/guy.svg',
            },
            topLevelDomains: [
              {
                name: '.gy',
              },
            ],
          },
          {
            name: 'Haiti',
            area: 27750,
            capital: 'Port-au-Prince',
            population: 11078033,
            populationDensity: 399.2083963963964,
            flag: {
              svgFile: 'https://restcountries.eu/data/hti.svg',
            },
            topLevelDomains: [
              {
                name: '.ht',
              },
            ],
          },
          {
            name: 'Heard Island and McDonald Islands',
            area: 412,
            capital: '',
            population: 0,
            populationDensity: 0,
            flag: {
              svgFile: 'https://restcountries.eu/data/hmd.svg',
            },
            topLevelDomains: [
              {
                name: '.aq',
              },
              {
                name: '.hm',
              },
            ],
          },
          {
            name: 'Holy See',
            area: 0.44,
            capital: 'Rome',
            population: 451,
            populationDensity: 1025,
            flag: {
              svgFile: 'https://restcountries.eu/data/vat.svg',
            },
            topLevelDomains: [
              {
                name: '.va',
              },
            ],
          },
          {
            name: 'Honduras',
            area: 112492,
            capital: 'Tegucigalpa',
            population: 8576532,
            populationDensity: 76.24126160082494,
            flag: {
              svgFile: 'https://restcountries.eu/data/hnd.svg',
            },
            topLevelDomains: [
              {
                name: '.hn',
              },
            ],
          },
          {
            name: 'Hong Kong',
            area: 1104,
            capital: 'City of Victoria',
            population: 7324300,
            populationDensity: 6634.329710144928,
            flag: {
              svgFile: 'https://restcountries.eu/data/hkg.svg',
            },
            topLevelDomains: [
              {
                name: '.hk',
              },
            ],
          },
          {
            name: 'Hungary',
            area: 93028,
            capital: 'Budapest',
            population: 9823000,
            populationDensity: 105.59186481489444,
            flag: {
              svgFile: 'https://restcountries.eu/data/hun.svg',
            },
            topLevelDomains: [
              {
                name: '.hu',
              },
            ],
          },
          {
            name: 'Iceland',
            area: 103000,
            capital: 'Reykjavík',
            population: 334300,
            populationDensity: 3.245631067961165,
            flag: {
              svgFile: 'https://restcountries.eu/data/isl.svg',
            },
            topLevelDomains: [
              {
                name: '.is',
              },
            ],
          },
          {
            name: 'India',
            area: 3287590,
            capital: 'New Delhi',
            population: 1295210000,
            populationDensity: 393.96944266164576,
            flag: {
              svgFile: 'https://restcountries.eu/data/ind.svg',
            },
            topLevelDomains: [
              {
                name: '.in',
              },
            ],
          },
          {
            name: 'Indonesia',
            area: 1904569,
            capital: 'Jakarta',
            population: 258705000,
            populationDensity: 135.83388157635665,
            flag: {
              svgFile: 'https://restcountries.eu/data/idn.svg',
            },
            topLevelDomains: [
              {
                name: '.id',
              },
            ],
          },
          {
            name: "Côte d'Ivoire",
            area: 322463,
            capital: 'Yamoussoukro',
            population: 22671331,
            populationDensity: 70.30676697791685,
            flag: {
              svgFile: 'https://restcountries.eu/data/civ.svg',
            },
            topLevelDomains: [
              {
                name: '.ci',
              },
            ],
          },
          {
            name: 'Iran (Islamic Republic of)',
            area: 1648195,
            capital: 'Tehran',
            population: 79369900,
            populationDensity: 48.15564905851553,
            flag: {
              svgFile: 'https://restcountries.eu/data/irn.svg',
            },
            topLevelDomains: [
              {
                name: '.ir',
              },
            ],
          },
          {
            name: 'Iraq',
            area: 438317,
            capital: 'Baghdad',
            population: 37883543,
            populationDensity: 86.42955440925175,
            flag: {
              svgFile: 'https://restcountries.eu/data/irq.svg',
            },
            topLevelDomains: [
              {
                name: '.iq',
              },
            ],
          },
          {
            name: 'Ireland',
            area: 70273,
            capital: 'Dublin',
            population: 6378000,
            populationDensity: 90.76032046447426,
            flag: {
              svgFile: 'https://restcountries.eu/data/irl.svg',
            },
            topLevelDomains: [
              {
                name: '.ie',
              },
            ],
          },
          {
            name: 'Isle of Man',
            area: 572,
            capital: 'Douglas',
            population: 84497,
            populationDensity: 147.72202797202797,
            flag: {
              svgFile: 'https://restcountries.eu/data/imn.svg',
            },
            topLevelDomains: [
              {
                name: '.im',
              },
            ],
          },
          {
            name: 'Israel',
            area: 20770,
            capital: 'Jerusalem',
            population: 8527400,
            populationDensity: 410.5633124699085,
            flag: {
              svgFile: 'https://restcountries.eu/data/isr.svg',
            },
            topLevelDomains: [
              {
                name: '.il',
              },
            ],
          },
          {
            name: 'Italy',
            area: 301336,
            capital: 'Rome',
            population: 60665551,
            populationDensity: 201.32194958451694,
            flag: {
              svgFile: 'https://restcountries.eu/data/ita.svg',
            },
            topLevelDomains: [
              {
                name: '.it',
              },
            ],
          },
          {
            name: 'Jamaica',
            area: 10991,
            capital: 'Kingston',
            population: 2723246,
            populationDensity: 247.77053953234466,
            flag: {
              svgFile: 'https://restcountries.eu/data/jam.svg',
            },
            topLevelDomains: [
              {
                name: '.jm',
              },
            ],
          },
          {
            name: 'Japan',
            area: 377930,
            capital: 'Tokyo',
            population: 126960000,
            populationDensity: 335.9352261000714,
            flag: {
              svgFile: 'https://restcountries.eu/data/jpn.svg',
            },
            topLevelDomains: [
              {
                name: '.jp',
              },
            ],
          },
          {
            name: 'Jersey',
            area: 116,
            capital: 'Saint Helier',
            population: 100800,
            populationDensity: 868.9655172413793,
            flag: {
              svgFile: 'https://restcountries.eu/data/jey.svg',
            },
            topLevelDomains: [
              {
                name: '.je',
              },
            ],
          },
          {
            name: 'Jordan',
            area: 89342,
            capital: 'Amman',
            population: 9531712,
            populationDensity: 106.6879183362808,
            flag: {
              svgFile: 'https://restcountries.eu/data/jor.svg',
            },
            topLevelDomains: [
              {
                name: '.jo',
              },
            ],
          },
          {
            name: 'Kazakhstan',
            area: 2724900,
            capital: 'Astana',
            population: 17753200,
            populationDensity: 6.515174868802525,
            flag: {
              svgFile: 'https://restcountries.eu/data/kaz.svg',
            },
            topLevelDomains: [
              {
                name: '.қаз',
              },
              {
                name: '.kz',
              },
            ],
          },
          {
            name: 'Kenya',
            area: 580367,
            capital: 'Nairobi',
            population: 47251000,
            populationDensity: 81.41572487753439,
            flag: {
              svgFile: 'https://restcountries.eu/data/ken.svg',
            },
            topLevelDomains: [
              {
                name: '.ke',
              },
            ],
          },
          {
            name: 'Kiribati',
            area: 811,
            capital: 'South Tarawa',
            population: 113400,
            populationDensity: 139.82737361282366,
            flag: {
              svgFile: 'https://restcountries.eu/data/kir.svg',
            },
            topLevelDomains: [
              {
                name: '.ki',
              },
            ],
          },
          {
            name: 'Kuwait',
            area: 17818,
            capital: 'Kuwait City',
            population: 4183658,
            populationDensity: 234.79952856661802,
            flag: {
              svgFile: 'https://restcountries.eu/data/kwt.svg',
            },
            topLevelDomains: [
              {
                name: '.kw',
              },
            ],
          },
          {
            name: 'Kyrgyzstan',
            area: 199951,
            capital: 'Bishkek',
            population: 6047800,
            populationDensity: 30.246410370540783,
            flag: {
              svgFile: 'https://restcountries.eu/data/kgz.svg',
            },
            topLevelDomains: [
              {
                name: '.kg',
              },
            ],
          },
          {
            name: "Lao People's Democratic Republic",
            area: 236800,
            capital: 'Vientiane',
            population: 6492400,
            populationDensity: 27.41722972972973,
            flag: {
              svgFile: 'https://restcountries.eu/data/lao.svg',
            },
            topLevelDomains: [
              {
                name: '.la',
              },
            ],
          },
          {
            name: 'Latvia',
            area: 64559,
            capital: 'Riga',
            population: 1961600,
            populationDensity: 30.384609427035734,
            flag: {
              svgFile: 'https://restcountries.eu/data/lva.svg',
            },
            topLevelDomains: [
              {
                name: '.lv',
              },
            ],
          },
          {
            name: 'Lebanon',
            area: 10452,
            capital: 'Beirut',
            population: 5988000,
            populationDensity: 572.9047072330654,
            flag: {
              svgFile: 'https://restcountries.eu/data/lbn.svg',
            },
            topLevelDomains: [
              {
                name: '.lb',
              },
            ],
          },
          {
            name: 'Lesotho',
            area: 30355,
            capital: 'Maseru',
            population: 1894194,
            populationDensity: 62.40138362707956,
            flag: {
              svgFile: 'https://restcountries.eu/data/lso.svg',
            },
            topLevelDomains: [
              {
                name: '.ls',
              },
            ],
          },
          {
            name: 'Liberia',
            area: 111369,
            capital: 'Monrovia',
            population: 4615000,
            populationDensity: 41.43882049762501,
            flag: {
              svgFile: 'https://restcountries.eu/data/lbr.svg',
            },
            topLevelDomains: [
              {
                name: '.lr',
              },
            ],
          },
          {
            name: 'Libya',
            area: 1759540,
            capital: 'Tripoli',
            population: 6385000,
            populationDensity: 3.6287893426691067,
            flag: {
              svgFile: 'https://restcountries.eu/data/lby.svg',
            },
            topLevelDomains: [
              {
                name: '.ly',
              },
            ],
          },
          {
            name: 'Liechtenstein',
            area: 160,
            capital: 'Vaduz',
            population: 37623,
            populationDensity: 235.14375,
            flag: {
              svgFile: 'https://restcountries.eu/data/lie.svg',
            },
            topLevelDomains: [
              {
                name: '.li',
              },
            ],
          },
          {
            name: 'Lithuania',
            area: 65300,
            capital: 'Vilnius',
            population: 2872294,
            populationDensity: 43.986125574272585,
            flag: {
              svgFile: 'https://restcountries.eu/data/ltu.svg',
            },
            topLevelDomains: [
              {
                name: '.lt',
              },
            ],
          },
          {
            name: 'Luxembourg',
            area: 2586,
            capital: 'Luxembourg',
            population: 576200,
            populationDensity: 222.81515854601702,
            flag: {
              svgFile: 'https://restcountries.eu/data/lux.svg',
            },
            topLevelDomains: [
              {
                name: '.lu',
              },
            ],
          },
          {
            name: 'Macao',
            area: 30,
            capital: '',
            population: 649100,
            populationDensity: 21636.666666666668,
            flag: {
              svgFile: 'https://restcountries.eu/data/mac.svg',
            },
            topLevelDomains: [
              {
                name: '.mo',
              },
            ],
          },
          {
            name: 'Macedonia (the former Yugoslav Republic of)',
            area: 25713,
            capital: 'Skopje',
            population: 2058539,
            populationDensity: 80.0582973593124,
            flag: {
              svgFile: 'https://restcountries.eu/data/mkd.svg',
            },
            topLevelDomains: [
              {
                name: '.mk',
              },
            ],
          },
          {
            name: 'Madagascar',
            area: 587041,
            capital: 'Antananarivo',
            population: 22434363,
            populationDensity: 38.21600705913215,
            flag: {
              svgFile: 'https://restcountries.eu/data/mdg.svg',
            },
            topLevelDomains: [
              {
                name: '.mg',
              },
            ],
          },
          {
            name: 'Malawi',
            area: 118484,
            capital: 'Lilongwe',
            population: 16832910,
            populationDensity: 142.06905573748355,
            flag: {
              svgFile: 'https://restcountries.eu/data/mwi.svg',
            },
            topLevelDomains: [
              {
                name: '.mw',
              },
            ],
          },
          {
            name: 'Malaysia',
            area: 330803,
            capital: 'Kuala Lumpur',
            population: 31405416,
            populationDensity: 94.93691411504733,
            flag: {
              svgFile: 'https://restcountries.eu/data/mys.svg',
            },
            topLevelDomains: [
              {
                name: '.my',
              },
            ],
          },
          {
            name: 'Maldives',
            area: 300,
            capital: 'Malé',
            population: 344023,
            populationDensity: 1146.7433333333333,
            flag: {
              svgFile: 'https://restcountries.eu/data/mdv.svg',
            },
            topLevelDomains: [
              {
                name: '.mv',
              },
            ],
          },
          {
            name: 'Mali',
            area: 1240192,
            capital: 'Bamako',
            population: 18135000,
            populationDensity: 14.62273583445144,
            flag: {
              svgFile: 'https://restcountries.eu/data/mli.svg',
            },
            topLevelDomains: [
              {
                name: '.ml',
              },
            ],
          },
          {
            name: 'Malta',
            area: 316,
            capital: 'Valletta',
            population: 425384,
            populationDensity: 1346.1518987341772,
            flag: {
              svgFile: 'https://restcountries.eu/data/mlt.svg',
            },
            topLevelDomains: [
              {
                name: '.mt',
              },
            ],
          },
          {
            name: 'Marshall Islands',
            area: 181,
            capital: 'Majuro',
            population: 54880,
            populationDensity: 303.2044198895028,
            flag: {
              svgFile: 'https://restcountries.eu/data/mhl.svg',
            },
            topLevelDomains: [
              {
                name: '.mh',
              },
            ],
          },
          {
            name: 'Martinique',
            area: null,
            capital: 'Fort-de-France',
            population: 378243,
            populationDensity: null,
            flag: {
              svgFile: 'https://restcountries.eu/data/mtq.svg',
            },
            topLevelDomains: [
              {
                name: '.mq',
              },
            ],
          },
          {
            name: 'Mauritania',
            area: 1030700,
            capital: 'Nouakchott',
            population: 3718678,
            populationDensity: 3.607915009217037,
            flag: {
              svgFile: 'https://restcountries.eu/data/mrt.svg',
            },
            topLevelDomains: [
              {
                name: '.mr',
              },
            ],
          },
          {
            name: 'Mauritius',
            area: 2040,
            capital: 'Port Louis',
            population: 1262879,
            populationDensity: 619.0583333333333,
            flag: {
              svgFile: 'https://restcountries.eu/data/mus.svg',
            },
            topLevelDomains: [
              {
                name: '.mu',
              },
            ],
          },
          {
            name: 'Mayotte',
            area: null,
            capital: 'Mamoudzou',
            population: 226915,
            populationDensity: null,
            flag: {
              svgFile: 'https://restcountries.eu/data/myt.svg',
            },
            topLevelDomains: [
              {
                name: '.yt',
              },
            ],
          },
          {
            name: 'Mexico',
            area: 1964375,
            capital: 'Mexico City',
            population: 122273473,
            populationDensity: 62.245484187082404,
            flag: {
              svgFile: 'https://restcountries.eu/data/mex.svg',
            },
            topLevelDomains: [
              {
                name: '.mx',
              },
            ],
          },
          {
            name: 'Micronesia (Federated States of)',
            area: 702,
            capital: 'Palikir',
            population: 102800,
            populationDensity: 146.43874643874645,
            flag: {
              svgFile: 'https://restcountries.eu/data/fsm.svg',
            },
            topLevelDomains: [
              {
                name: '.fm',
              },
            ],
          },
          {
            name: 'Moldova (Republic of)',
            area: 33846,
            capital: 'Chișinău',
            population: 3553100,
            populationDensity: 104.97843172014419,
            flag: {
              svgFile: 'https://restcountries.eu/data/mda.svg',
            },
            topLevelDomains: [
              {
                name: '.md',
              },
            ],
          },
          {
            name: 'Monaco',
            area: 2.02,
            capital: 'Monaco',
            population: 38400,
            populationDensity: 19009.90099009901,
            flag: {
              svgFile: 'https://restcountries.eu/data/mco.svg',
            },
            topLevelDomains: [
              {
                name: '.mc',
              },
            ],
          },
          {
            name: 'Mongolia',
            area: 1564110,
            capital: 'Ulan Bator',
            population: 3093100,
            populationDensity: 1.9775463362551227,
            flag: {
              svgFile: 'https://restcountries.eu/data/mng.svg',
            },
            topLevelDomains: [
              {
                name: '.mn',
              },
            ],
          },
          {
            name: 'Montenegro',
            area: 13812,
            capital: 'Podgorica',
            population: 621810,
            populationDensity: 45.01954821894005,
            flag: {
              svgFile: 'https://restcountries.eu/data/mne.svg',
            },
            topLevelDomains: [
              {
                name: '.me',
              },
            ],
          },
          {
            name: 'Montserrat',
            area: 102,
            capital: 'Plymouth',
            population: 4922,
            populationDensity: 48.254901960784316,
            flag: {
              svgFile: 'https://restcountries.eu/data/msr.svg',
            },
            topLevelDomains: [
              {
                name: '.ms',
              },
            ],
          },
          {
            name: 'Morocco',
            area: 446550,
            capital: 'Rabat',
            population: 33337529,
            populationDensity: 74.65575859366253,
            flag: {
              svgFile: 'https://restcountries.eu/data/mar.svg',
            },
            topLevelDomains: [
              {
                name: '.ma',
              },
            ],
          },
          {
            name: 'Mozambique',
            area: 801590,
            capital: 'Maputo',
            population: 26423700,
            populationDensity: 32.96410883369303,
            flag: {
              svgFile: 'https://restcountries.eu/data/moz.svg',
            },
            topLevelDomains: [
              {
                name: '.mz',
              },
            ],
          },
          {
            name: 'Myanmar',
            area: 676578,
            capital: 'Naypyidaw',
            population: 51419420,
            populationDensity: 75.99924916269816,
            flag: {
              svgFile: 'https://restcountries.eu/data/mmr.svg',
            },
            topLevelDomains: [
              {
                name: '.mm',
              },
            ],
          },
          {
            name: 'Namibia',
            area: 825615,
            capital: 'Windhoek',
            population: 2324388,
            populationDensity: 2.81534129103759,
            flag: {
              svgFile: 'https://restcountries.eu/data/nam.svg',
            },
            topLevelDomains: [
              {
                name: '.na',
              },
            ],
          },
          {
            name: 'Nauru',
            area: 21,
            capital: 'Yaren',
            population: 10084,
            populationDensity: 480.1904761904762,
            flag: {
              svgFile: 'https://restcountries.eu/data/nru.svg',
            },
            topLevelDomains: [
              {
                name: '.nr',
              },
            ],
          },
          {
            name: 'Nepal',
            area: 147181,
            capital: 'Kathmandu',
            population: 28431500,
            populationDensity: 193.17371128066802,
            flag: {
              svgFile: 'https://restcountries.eu/data/npl.svg',
            },
            topLevelDomains: [
              {
                name: '.np',
              },
            ],
          },
          {
            name: 'Netherlands',
            area: 41850,
            capital: 'Amsterdam',
            population: 17019800,
            populationDensity: 406.6857825567503,
            flag: {
              svgFile: 'https://restcountries.eu/data/nld.svg',
            },
            topLevelDomains: [
              {
                name: '.nl',
              },
            ],
          },
          {
            name: 'New Caledonia',
            area: 18575,
            capital: 'Nouméa',
            population: 268767,
            populationDensity: 14.4692866756393,
            flag: {
              svgFile: 'https://restcountries.eu/data/ncl.svg',
            },
            topLevelDomains: [
              {
                name: '.nc',
              },
            ],
          },
          {
            name: 'New Zealand',
            area: 270467,
            capital: 'Wellington',
            population: 4697854,
            populationDensity: 17.369416601655654,
            flag: {
              svgFile: 'https://restcountries.eu/data/nzl.svg',
            },
            topLevelDomains: [
              {
                name: '.nz',
              },
            ],
          },
          {
            name: 'Nicaragua',
            area: 130373,
            capital: 'Managua',
            population: 6262703,
            populationDensity: 48.03680976889387,
            flag: {
              svgFile: 'https://restcountries.eu/data/nic.svg',
            },
            topLevelDomains: [
              {
                name: '.ni',
              },
            ],
          },
          {
            name: 'Niger',
            area: 1267000,
            capital: 'Niamey',
            population: 20715000,
            populationDensity: 16.349644830307813,
            flag: {
              svgFile: 'https://restcountries.eu/data/ner.svg',
            },
            topLevelDomains: [
              {
                name: '.ne',
              },
            ],
          },
          {
            name: 'Nigeria',
            area: 923768,
            capital: 'Abuja',
            population: 186988000,
            populationDensity: 202.41878913320227,
            flag: {
              svgFile: 'https://restcountries.eu/data/nga.svg',
            },
            topLevelDomains: [
              {
                name: '.ng',
              },
            ],
          },
          {
            name: 'Niue',
            area: 260,
            capital: 'Alofi',
            population: 1470,
            populationDensity: 5.653846153846154,
            flag: {
              svgFile: 'https://restcountries.eu/data/niu.svg',
            },
            topLevelDomains: [
              {
                name: '.nu',
              },
            ],
          },
          {
            name: 'Norfolk Island',
            area: 36,
            capital: 'Kingston',
            population: 2302,
            populationDensity: 63.94444444444444,
            flag: {
              svgFile: 'https://restcountries.eu/data/nfk.svg',
            },
            topLevelDomains: [
              {
                name: '.nf',
              },
            ],
          },
          {
            name: "Korea (Democratic People's Republic of)",
            area: 120538,
            capital: 'Pyongyang',
            population: 25281000,
            populationDensity: 209.73468947551808,
            flag: {
              svgFile: 'https://restcountries.eu/data/prk.svg',
            },
            topLevelDomains: [
              {
                name: '.kp',
              },
            ],
          },
          {
            name: 'Northern Mariana Islands',
            area: 464,
            capital: 'Saipan',
            population: 56940,
            populationDensity: 122.71551724137932,
            flag: {
              svgFile: 'https://restcountries.eu/data/mnp.svg',
            },
            topLevelDomains: [
              {
                name: '.mp',
              },
            ],
          },
          {
            name: 'Norway',
            area: 323802,
            capital: 'Oslo',
            population: 5223256,
            populationDensity: 16.131018338367273,
            flag: {
              svgFile: 'https://restcountries.eu/data/nor.svg',
            },
            topLevelDomains: [
              {
                name: '.no',
              },
            ],
          },
          {
            name: 'Oman',
            area: 309500,
            capital: 'Muscat',
            population: 4420133,
            populationDensity: 14.281528271405493,
            flag: {
              svgFile: 'https://restcountries.eu/data/omn.svg',
            },
            topLevelDomains: [
              {
                name: '.om',
              },
            ],
          },
          {
            name: 'Pakistan',
            area: 881912,
            capital: 'Islamabad',
            population: 194125062,
            populationDensity: 220.1184041038108,
            flag: {
              svgFile: 'https://restcountries.eu/data/pak.svg',
            },
            topLevelDomains: [
              {
                name: '.pk',
              },
            ],
          },
          {
            name: 'Palau',
            area: 459,
            capital: 'Ngerulmud',
            population: 17950,
            populationDensity: 39.106753812636164,
            flag: {
              svgFile: 'https://restcountries.eu/data/plw.svg',
            },
            topLevelDomains: [
              {
                name: '.pw',
              },
            ],
          },
          {
            name: 'Palestine, State of',
            area: null,
            capital: 'Ramallah',
            population: 4682467,
            populationDensity: null,
            flag: {
              svgFile: 'https://restcountries.eu/data/pse.svg',
            },
            topLevelDomains: [
              {
                name: '.ps',
              },
            ],
          },
          {
            name: 'Panama',
            area: 75417,
            capital: 'Panama City',
            population: 3814672,
            populationDensity: 50.58106262513757,
            flag: {
              svgFile: 'https://restcountries.eu/data/pan.svg',
            },
            topLevelDomains: [
              {
                name: '.pa',
              },
            ],
          },
          {
            name: 'Papua New Guinea',
            area: 462840,
            capital: 'Port Moresby',
            population: 8083700,
            populationDensity: 17.465430818425375,
            flag: {
              svgFile: 'https://restcountries.eu/data/png.svg',
            },
            topLevelDomains: [
              {
                name: '.pg',
              },
            ],
          },
          {
            name: 'Paraguay',
            area: 406752,
            capital: 'Asunción',
            population: 6854536,
            populationDensity: 16.851880261191095,
            flag: {
              svgFile: 'https://restcountries.eu/data/pry.svg',
            },
            topLevelDomains: [
              {
                name: '.py',
              },
            ],
          },
          {
            name: 'Peru',
            area: 1285216,
            capital: 'Lima',
            population: 31488700,
            populationDensity: 24.500706496028684,
            flag: {
              svgFile: 'https://restcountries.eu/data/per.svg',
            },
            topLevelDomains: [
              {
                name: '.pe',
              },
            ],
          },
          {
            name: 'Philippines',
            area: 342353,
            capital: 'Manila',
            population: 103279800,
            populationDensity: 301.67633991815467,
            flag: {
              svgFile: 'https://restcountries.eu/data/phl.svg',
            },
            topLevelDomains: [
              {
                name: '.ph',
              },
            ],
          },
          {
            name: 'Pitcairn',
            area: 47,
            capital: 'Adamstown',
            population: 56,
            populationDensity: 1.1914893617021276,
            flag: {
              svgFile: 'https://restcountries.eu/data/pcn.svg',
            },
            topLevelDomains: [
              {
                name: '.pn',
              },
            ],
          },
          {
            name: 'Poland',
            area: 312679,
            capital: 'Warsaw',
            population: 38437239,
            populationDensity: 122.92875121130616,
            flag: {
              svgFile: 'https://restcountries.eu/data/pol.svg',
            },
            topLevelDomains: [
              {
                name: '.pl',
              },
            ],
          },
          {
            name: 'Portugal',
            area: 92090,
            capital: 'Lisbon',
            population: 10374822,
            populationDensity: 112.65959387555652,
            flag: {
              svgFile: 'https://restcountries.eu/data/prt.svg',
            },
            topLevelDomains: [
              {
                name: '.pt',
              },
            ],
          },
          {
            name: 'Puerto Rico',
            area: 8870,
            capital: 'San Juan',
            population: 3474182,
            populationDensity: 391.67779030439686,
            flag: {
              svgFile: 'https://restcountries.eu/data/pri.svg',
            },
            topLevelDomains: [
              {
                name: '.pr',
              },
            ],
          },
          {
            name: 'Qatar',
            area: 11586,
            capital: 'Doha',
            population: 2587564,
            populationDensity: 223.3354047988952,
            flag: {
              svgFile: 'https://restcountries.eu/data/qat.svg',
            },
            topLevelDomains: [
              {
                name: '.qa',
              },
            ],
          },
          {
            name: 'Republic of Kosovo',
            area: 10908,
            capital: 'Pristina',
            population: 1733842,
            populationDensity: 158.95141180784745,
            flag: {
              svgFile: 'https://restcountries.eu/data/kos.svg',
            },
            topLevelDomains: [],
          },
          {
            name: 'Réunion',
            area: null,
            capital: 'Saint-Denis',
            population: 840974,
            populationDensity: null,
            flag: {
              svgFile: 'https://restcountries.eu/data/reu.svg',
            },
            topLevelDomains: [
              {
                name: '.re',
              },
            ],
          },
          {
            name: 'Romania',
            area: 238391,
            capital: 'Bucharest',
            population: 19861408,
            populationDensity: 83.31442042694565,
            flag: {
              svgFile: 'https://restcountries.eu/data/rou.svg',
            },
            topLevelDomains: [
              {
                name: '.ro',
              },
            ],
          },
          {
            name: 'Russian Federation',
            area: 17124442,
            capital: 'Moscow',
            population: 146599183,
            populationDensity: 8.560815178678522,
            flag: {
              svgFile: 'https://restcountries.eu/data/rus.svg',
            },
            topLevelDomains: [
              {
                name: '.ru',
              },
            ],
          },
          {
            name: 'Rwanda',
            area: 26338,
            capital: 'Kigali',
            population: 11553188,
            populationDensity: 438.65092262130764,
            flag: {
              svgFile: 'https://restcountries.eu/data/rwa.svg',
            },
            topLevelDomains: [
              {
                name: '.rw',
              },
            ],
          },
          {
            name: 'Saint Barthélemy',
            area: 21,
            capital: 'Gustavia',
            population: 9417,
            populationDensity: 448.42857142857144,
            flag: {
              svgFile: 'https://restcountries.eu/data/blm.svg',
            },
            topLevelDomains: [
              {
                name: '.bl',
              },
            ],
          },
          {
            name: 'Saint Helena, Ascension and Tristan da Cunha',
            area: null,
            capital: 'Jamestown',
            population: 4255,
            populationDensity: null,
            flag: {
              svgFile: 'https://restcountries.eu/data/shn.svg',
            },
            topLevelDomains: [
              {
                name: '.sh',
              },
            ],
          },
          {
            name: 'Saint Kitts and Nevis',
            area: 261,
            capital: 'Basseterre',
            population: 46204,
            populationDensity: 177.02681992337165,
            flag: {
              svgFile: 'https://restcountries.eu/data/kna.svg',
            },
            topLevelDomains: [
              {
                name: '.kn',
              },
            ],
          },
          {
            name: 'Saint Lucia',
            area: 616,
            capital: 'Castries',
            population: 186000,
            populationDensity: 301.94805194805195,
            flag: {
              svgFile: 'https://restcountries.eu/data/lca.svg',
            },
            topLevelDomains: [
              {
                name: '.lc',
              },
            ],
          },
          {
            name: 'Saint Martin (French part)',
            area: 53,
            capital: 'Marigot',
            population: 36979,
            populationDensity: 697.7169811320755,
            flag: {
              svgFile: 'https://restcountries.eu/data/maf.svg',
            },
            topLevelDomains: [
              {
                name: '.gp',
              },
              {
                name: '.fr',
              },
              {
                name: '.mf',
              },
            ],
          },
          {
            name: 'Saint Pierre and Miquelon',
            area: 242,
            capital: 'Saint-Pierre',
            population: 6069,
            populationDensity: 25.078512396694215,
            flag: {
              svgFile: 'https://restcountries.eu/data/spm.svg',
            },
            topLevelDomains: [
              {
                name: '.pm',
              },
            ],
          },
          {
            name: 'Saint Vincent and the Grenadines',
            area: 389,
            capital: 'Kingstown',
            population: 109991,
            populationDensity: 282.75321336760925,
            flag: {
              svgFile: 'https://restcountries.eu/data/vct.svg',
            },
            topLevelDomains: [
              {
                name: '.vc',
              },
            ],
          },
          {
            name: 'Samoa',
            area: 2842,
            capital: 'Apia',
            population: 194899,
            populationDensity: 68.57811400422239,
            flag: {
              svgFile: 'https://restcountries.eu/data/wsm.svg',
            },
            topLevelDomains: [
              {
                name: '.ws',
              },
            ],
          },
          {
            name: 'San Marino',
            area: 61,
            capital: 'City of San Marino',
            population: 33005,
            populationDensity: 541.0655737704918,
            flag: {
              svgFile: 'https://restcountries.eu/data/smr.svg',
            },
            topLevelDomains: [
              {
                name: '.sm',
              },
            ],
          },
          {
            name: 'Sao Tome and Principe',
            area: 964,
            capital: 'São Tomé',
            population: 187356,
            populationDensity: 194.35269709543567,
            flag: {
              svgFile: 'https://restcountries.eu/data/stp.svg',
            },
            topLevelDomains: [
              {
                name: '.st',
              },
            ],
          },
          {
            name: 'Saudi Arabia',
            area: 2149690,
            capital: 'Riyadh',
            population: 32248200,
            populationDensity: 15.001325772553251,
            flag: {
              svgFile: 'https://restcountries.eu/data/sau.svg',
            },
            topLevelDomains: [
              {
                name: '.sa',
              },
            ],
          },
          {
            name: 'Senegal',
            area: 196722,
            capital: 'Dakar',
            population: 14799859,
            populationDensity: 75.2323532700969,
            flag: {
              svgFile: 'https://restcountries.eu/data/sen.svg',
            },
            topLevelDomains: [
              {
                name: '.sn',
              },
            ],
          },
          {
            name: 'Serbia',
            area: 88361,
            capital: 'Belgrade',
            population: 7076372,
            populationDensity: 80.08478853792963,
            flag: {
              svgFile: 'https://restcountries.eu/data/srb.svg',
            },
            topLevelDomains: [
              {
                name: '.rs',
              },
            ],
          },
          {
            name: 'Seychelles',
            area: 452,
            capital: 'Victoria',
            population: 91400,
            populationDensity: 202.21238938053096,
            flag: {
              svgFile: 'https://restcountries.eu/data/syc.svg',
            },
            topLevelDomains: [
              {
                name: '.sc',
              },
            ],
          },
          {
            name: 'Sierra Leone',
            area: 71740,
            capital: 'Freetown',
            population: 7075641,
            populationDensity: 98.62895177028157,
            flag: {
              svgFile: 'https://restcountries.eu/data/sle.svg',
            },
            topLevelDomains: [
              {
                name: '.sl',
              },
            ],
          },
          {
            name: 'Singapore',
            area: 710,
            capital: 'Singapore',
            population: 5535000,
            populationDensity: 7795.774647887324,
            flag: {
              svgFile: 'https://restcountries.eu/data/sgp.svg',
            },
            topLevelDomains: [
              {
                name: '.sg',
              },
            ],
          },
          {
            name: 'Sint Maarten (Dutch part)',
            area: 34,
            capital: 'Philipsburg',
            population: 38247,
            populationDensity: 1124.9117647058824,
            flag: {
              svgFile: 'https://restcountries.eu/data/sxm.svg',
            },
            topLevelDomains: [
              {
                name: '.sx',
              },
            ],
          },
          {
            name: 'Slovakia',
            area: 49037,
            capital: 'Bratislava',
            population: 5426252,
            populationDensity: 110.65627995187307,
            flag: {
              svgFile: 'https://restcountries.eu/data/svk.svg',
            },
            topLevelDomains: [
              {
                name: '.sk',
              },
            ],
          },
          {
            name: 'Slovenia',
            area: 20273,
            capital: 'Ljubljana',
            population: 2064188,
            populationDensity: 101.81956296552065,
            flag: {
              svgFile: 'https://restcountries.eu/data/svn.svg',
            },
            topLevelDomains: [
              {
                name: '.si',
              },
            ],
          },
          {
            name: 'Solomon Islands',
            area: 28896,
            capital: 'Honiara',
            population: 642000,
            populationDensity: 22.217607973421927,
            flag: {
              svgFile: 'https://restcountries.eu/data/slb.svg',
            },
            topLevelDomains: [
              {
                name: '.sb',
              },
            ],
          },
          {
            name: 'Somalia',
            area: 637657,
            capital: 'Mogadishu',
            population: 11079000,
            populationDensity: 17.374544621951927,
            flag: {
              svgFile: 'https://restcountries.eu/data/som.svg',
            },
            topLevelDomains: [
              {
                name: '.so',
              },
            ],
          },
          {
            name: 'South Africa',
            area: 1221037,
            capital: 'Pretoria',
            population: 55653654,
            populationDensity: 45.57900702435717,
            flag: {
              svgFile: 'https://restcountries.eu/data/zaf.svg',
            },
            topLevelDomains: [
              {
                name: '.za',
              },
            ],
          },
          {
            name: 'South Georgia and the South Sandwich Islands',
            area: null,
            capital: 'King Edward Point',
            population: 30,
            populationDensity: null,
            flag: {
              svgFile: 'https://restcountries.eu/data/sgs.svg',
            },
            topLevelDomains: [
              {
                name: '.gs',
              },
            ],
          },
          {
            name: 'Korea (Republic of)',
            area: 100210,
            capital: 'Seoul',
            population: 50801405,
            populationDensity: 506.9494561421016,
            flag: {
              svgFile: 'https://restcountries.eu/data/kor.svg',
            },
            topLevelDomains: [
              {
                name: '.kr',
              },
            ],
          },
          {
            name: 'South Sudan',
            area: 619745,
            capital: 'Juba',
            population: 12131000,
            populationDensity: 19.574179702942338,
            flag: {
              svgFile: 'https://restcountries.eu/data/ssd.svg',
            },
            topLevelDomains: [
              {
                name: '.ss',
              },
            ],
          },
          {
            name: 'Spain',
            area: 505992,
            capital: 'Madrid',
            population: 46438422,
            populationDensity: 91.77698856898924,
            flag: {
              svgFile: 'https://restcountries.eu/data/esp.svg',
            },
            topLevelDomains: [
              {
                name: '.es',
              },
            ],
          },
          {
            name: 'Sri Lanka',
            area: 65610,
            capital: 'Colombo',
            population: 20966000,
            populationDensity: 319.55494589239447,
            flag: {
              svgFile: 'https://restcountries.eu/data/lka.svg',
            },
            topLevelDomains: [
              {
                name: '.lk',
              },
            ],
          },
          {
            name: 'Sudan',
            area: 1886068,
            capital: 'Khartoum',
            population: 39598700,
            populationDensity: 20.995372383180246,
            flag: {
              svgFile: 'https://restcountries.eu/data/sdn.svg',
            },
            topLevelDomains: [
              {
                name: '.sd',
              },
            ],
          },
          {
            name: 'Suriname',
            area: 163820,
            capital: 'Paramaribo',
            population: 541638,
            populationDensity: 3.3062995971187887,
            flag: {
              svgFile: 'https://restcountries.eu/data/sur.svg',
            },
            topLevelDomains: [
              {
                name: '.sr',
              },
            ],
          },
          {
            name: 'Svalbard and Jan Mayen',
            area: null,
            capital: 'Longyearbyen',
            population: 2562,
            populationDensity: null,
            flag: {
              svgFile: 'https://restcountries.eu/data/sjm.svg',
            },
            topLevelDomains: [
              {
                name: '.sj',
              },
            ],
          },
          {
            name: 'Swaziland',
            area: 17364,
            capital: 'Lobamba',
            population: 1132657,
            populationDensity: 65.23018889656761,
            flag: {
              svgFile: 'https://restcountries.eu/data/swz.svg',
            },
            topLevelDomains: [
              {
                name: '.sz',
              },
            ],
          },
          {
            name: 'Sweden',
            area: 450295,
            capital: 'Stockholm',
            population: 9894888,
            populationDensity: 21.97423466838406,
            flag: {
              svgFile: 'https://restcountries.eu/data/swe.svg',
            },
            topLevelDomains: [
              {
                name: '.se',
              },
            ],
          },
          {
            name: 'Switzerland',
            area: 41284,
            capital: 'Bern',
            population: 8341600,
            populationDensity: 202.05406452863096,
            flag: {
              svgFile: 'https://restcountries.eu/data/che.svg',
            },
            topLevelDomains: [
              {
                name: '.ch',
              },
            ],
          },
          {
            name: 'Syrian Arab Republic',
            area: 185180,
            capital: 'Damascus',
            population: 18564000,
            populationDensity: 100.24840695539476,
            flag: {
              svgFile: 'https://restcountries.eu/data/syr.svg',
            },
            topLevelDomains: [
              {
                name: '.sy',
              },
            ],
          },
          {
            name: 'Taiwan',
            area: 36193,
            capital: 'Taipei',
            population: 23503349,
            populationDensity: 649.3893570579946,
            flag: {
              svgFile: 'https://restcountries.eu/data/twn.svg',
            },
            topLevelDomains: [
              {
                name: '.tw',
              },
            ],
          },
          {
            name: 'Tajikistan',
            area: 143100,
            capital: 'Dushanbe',
            population: 8593600,
            populationDensity: 60.053109713487075,
            flag: {
              svgFile: 'https://restcountries.eu/data/tjk.svg',
            },
            topLevelDomains: [
              {
                name: '.tj',
              },
            ],
          },
          {
            name: 'Tanzania, United Republic of',
            area: 945087,
            capital: 'Dodoma',
            population: 55155000,
            populationDensity: 58.359706566697035,
            flag: {
              svgFile: 'https://restcountries.eu/data/tza.svg',
            },
            topLevelDomains: [
              {
                name: '.tz',
              },
            ],
          },
          {
            name: 'Thailand',
            area: 513120,
            capital: 'Bangkok',
            population: 65327652,
            populationDensity: 127.31456969130028,
            flag: {
              svgFile: 'https://restcountries.eu/data/tha.svg',
            },
            topLevelDomains: [
              {
                name: '.th',
              },
            ],
          },
          {
            name: 'Timor-Leste',
            area: 14874,
            capital: 'Dili',
            population: 1167242,
            populationDensity: 78.47532607234099,
            flag: {
              svgFile: 'https://restcountries.eu/data/tls.svg',
            },
            topLevelDomains: [
              {
                name: '.tl',
              },
            ],
          },
          {
            name: 'Togo',
            area: 56785,
            capital: 'Lomé',
            population: 7143000,
            populationDensity: 125.79026151272343,
            flag: {
              svgFile: 'https://restcountries.eu/data/tgo.svg',
            },
            topLevelDomains: [
              {
                name: '.tg',
              },
            ],
          },
          {
            name: 'Tokelau',
            area: 12,
            capital: 'Fakaofo',
            population: 1411,
            populationDensity: 117.58333333333333,
            flag: {
              svgFile: 'https://restcountries.eu/data/tkl.svg',
            },
            topLevelDomains: [
              {
                name: '.tk',
              },
            ],
          },
          {
            name: 'Tonga',
            area: 747,
            capital: "Nuku'alofa",
            population: 103252,
            populationDensity: 138.22222222222223,
            flag: {
              svgFile: 'https://restcountries.eu/data/ton.svg',
            },
            topLevelDomains: [
              {
                name: '.to',
              },
            ],
          },
          {
            name: 'Trinidad and Tobago',
            area: 5130,
            capital: 'Port of Spain',
            population: 1349667,
            populationDensity: 263.09298245614036,
            flag: {
              svgFile: 'https://restcountries.eu/data/tto.svg',
            },
            topLevelDomains: [
              {
                name: '.tt',
              },
            ],
          },
          {
            name: 'Tunisia',
            area: 163610,
            capital: 'Tunis',
            population: 11154400,
            populationDensity: 68.17676181162521,
            flag: {
              svgFile: 'https://restcountries.eu/data/tun.svg',
            },
            topLevelDomains: [
              {
                name: '.tn',
              },
            ],
          },
          {
            name: 'Turkey',
            area: 783562,
            capital: 'Ankara',
            population: 78741053,
            populationDensity: 100.49115832569727,
            flag: {
              svgFile: 'https://restcountries.eu/data/tur.svg',
            },
            topLevelDomains: [
              {
                name: '.tr',
              },
            ],
          },
          {
            name: 'Turkmenistan',
            area: 488100,
            capital: 'Ashgabat',
            population: 4751120,
            populationDensity: 9.733906986273304,
            flag: {
              svgFile: 'https://restcountries.eu/data/tkm.svg',
            },
            topLevelDomains: [
              {
                name: '.tm',
              },
            ],
          },
          {
            name: 'Turks and Caicos Islands',
            area: 948,
            capital: 'Cockburn Town',
            population: 31458,
            populationDensity: 33.18354430379747,
            flag: {
              svgFile: 'https://restcountries.eu/data/tca.svg',
            },
            topLevelDomains: [
              {
                name: '.tc',
              },
            ],
          },
          {
            name: 'Tuvalu',
            area: 26,
            capital: 'Funafuti',
            population: 10640,
            populationDensity: 409.2307692307692,
            flag: {
              svgFile: 'https://restcountries.eu/data/tuv.svg',
            },
            topLevelDomains: [
              {
                name: '.tv',
              },
            ],
          },
          {
            name: 'Uganda',
            area: 241550,
            capital: 'Kampala',
            population: 33860700,
            populationDensity: 140.18091492444628,
            flag: {
              svgFile: 'https://restcountries.eu/data/uga.svg',
            },
            topLevelDomains: [
              {
                name: '.ug',
              },
            ],
          },
          {
            name: 'Ukraine',
            area: 603700,
            capital: 'Kiev',
            population: 42692393,
            populationDensity: 70.71789464966042,
            flag: {
              svgFile: 'https://restcountries.eu/data/ukr.svg',
            },
            topLevelDomains: [
              {
                name: '.ua',
              },
            ],
          },
          {
            name: 'United Arab Emirates',
            area: 83600,
            capital: 'Abu Dhabi',
            population: 9856000,
            populationDensity: 117.89473684210526,
            flag: {
              svgFile: 'https://restcountries.eu/data/are.svg',
            },
            topLevelDomains: [
              {
                name: '.ae',
              },
            ],
          },
          {
            name:
              'United Kingdom of Great Britain and Northern Ireland',
            area: 242900,
            capital: 'London',
            population: 65110000,
            populationDensity: 268.05269658295595,
            flag: {
              svgFile: 'https://restcountries.eu/data/gbr.svg',
            },
            topLevelDomains: [
              {
                name: '.uk',
              },
            ],
          },
          {
            name: 'United States of America',
            area: 9629091,
            capital: 'Washington, D.C.',
            population: 323947000,
            populationDensity: 33.64253178207579,
            flag: {
              svgFile: 'https://restcountries.eu/data/usa.svg',
            },
            topLevelDomains: [
              {
                name: '.us',
              },
            ],
          },
          {
            name: 'Uruguay',
            area: 181034,
            capital: 'Montevideo',
            population: 3480222,
            populationDensity: 19.22413469293061,
            flag: {
              svgFile: 'https://restcountries.eu/data/ury.svg',
            },
            topLevelDomains: [
              {
                name: '.uy',
              },
            ],
          },
          {
            name: 'Uzbekistan',
            area: 447400,
            capital: 'Tashkent',
            population: 31576400,
            populationDensity: 70.5775592311131,
            flag: {
              svgFile: 'https://restcountries.eu/data/uzb.svg',
            },
            topLevelDomains: [
              {
                name: '.uz',
              },
            ],
          },
          {
            name: 'Vanuatu',
            area: 12189,
            capital: 'Port Vila',
            population: 277500,
            populationDensity: 22.76642874723111,
            flag: {
              svgFile: 'https://restcountries.eu/data/vut.svg',
            },
            topLevelDomains: [
              {
                name: '.vu',
              },
            ],
          },
          {
            name: 'Venezuela (Bolivarian Republic of)',
            area: 916445,
            capital: 'Caracas',
            population: 31028700,
            populationDensity: 33.85767831130073,
            flag: {
              svgFile: 'https://restcountries.eu/data/ven.svg',
            },
            topLevelDomains: [
              {
                name: '.ve',
              },
            ],
          },
          {
            name: 'Viet Nam',
            area: 331212,
            capital: 'Hanoi',
            population: 92700000,
            populationDensity: 279.881163725952,
            flag: {
              svgFile: 'https://restcountries.eu/data/vnm.svg',
            },
            topLevelDomains: [
              {
                name: '.vn',
              },
            ],
          },
          {
            name: 'Wallis and Futuna',
            area: 142,
            capital: 'Mata-Utu',
            population: 11750,
            populationDensity: 82.74647887323944,
            flag: {
              svgFile: 'https://restcountries.eu/data/wlf.svg',
            },
            topLevelDomains: [
              {
                name: '.wf',
              },
            ],
          },
          {
            name: 'Western Sahara',
            area: 266000,
            capital: 'El Aaiún',
            population: 510713,
            populationDensity: 1.9199736842105264,
            flag: {
              svgFile: 'https://restcountries.eu/data/esh.svg',
            },
            topLevelDomains: [
              {
                name: '.eh',
              },
            ],
          },
          {
            name: 'Yemen',
            area: 527968,
            capital: "Sana'a",
            population: 27478000,
            populationDensity: 52.04482089823626,
            flag: {
              svgFile: 'https://restcountries.eu/data/yem.svg',
            },
            topLevelDomains: [
              {
                name: '.ye',
              },
            ],
          },
          {
            name: 'Zambia',
            area: 752612,
            capital: 'Lusaka',
            population: 15933883,
            populationDensity: 21.171444250158117,
            flag: {
              svgFile: 'https://restcountries.eu/data/zmb.svg',
            },
            topLevelDomains: [
              {
                name: '.zm',
              },
            ],
          },
          {
            name: 'Zimbabwe',
            area: 390757,
            capital: 'Harare',
            population: 14240168,
            populationDensity: 36.44251542518752,
            flag: {
              svgFile: 'https://restcountries.eu/data/zwe.svg',
            },
            topLevelDomains: [
              {
                name: '.zw',
              },
            ],
          },
        ],
      },
    },
  },
];

// should render with no errors
describe('render tests', () => {
  test('should render header', async () => {
    const { container } = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Home />
      </MockedProvider>,
    );
    const header = await findByTestId(container, 'header-component');
    expect(header).toBeTruthy();
  });
});


// should fire a query

// while fetching
// should render skeleton component

// if query is ok:
// should render cards correctly

// if query is not ok:
// should render error message
