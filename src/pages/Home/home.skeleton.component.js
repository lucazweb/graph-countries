import React from 'react';
import { Row, Col } from 'react-flexbox-grid';
import { CardSkeleton } from '../../components';

export const HomeSkeleton = () => {
  return (
    <>
      <Row>
        <Col xs={12} md={3}>
          <CardSkeleton active loading={true} />
        </Col>
        <Col xs={12} md={3}>
          <CardSkeleton active loading={true} />
        </Col>
        <Col xs={12} md={3}>
          <CardSkeleton active loading={true} />
        </Col>
        <Col xs={12} md={3}>
          <CardSkeleton active loading={true} />
        </Col>
      </Row>
      <Row>
        <Col xs={12} md={3}>
          <CardSkeleton active loading={true} />
        </Col>
        <Col xs={12} md={3}>
          <CardSkeleton active loading={true} />
        </Col>
        <Col xs={12} md={3}>
          <CardSkeleton active loading={true} />
        </Col>
        <Col xs={12} md={3}>
          <CardSkeleton active loading={true} />
        </Col>
      </Row>
    </>
  );
};
