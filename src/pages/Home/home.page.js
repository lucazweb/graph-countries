import React, { useEffect } from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { Form, Input } from 'antd';
import {
  Header,
  Card,
  Placeholder404,
  PlaceholderError,
} from '../../components';
import { HomeSkeleton } from './home.skeleton.component';
import { handleCountryData } from '../../helpers/dataHandlers';
import { useHistory } from 'react-router-dom';
import { getCountries, searchCountry } from '../../store/country';
import { useDispatch, useSelector } from 'react-redux';

export const Home = () => {
  const { countries, loading, error } = useSelector(
    ({ country: { countries, loading, error } }) => ({
      countries,
      loading,
      error,
    }),
  );

  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(getCountries());
  }, []);

  const handleRedirect = (id) => {
    history.push(`/detail/${id}`);
  };

  const handleSearch = async ({ q }) => {
    dispatch(searchCountry(q));
  };

  if (error) {
    console.log('from home', error);
    return <PlaceholderError />;
  }

  return (
    <>
      <Header />
      <Grid>
        <Row>
          <Col xs={12}>
            <Form
              name="basic"
              initialValues={{ q: '' }}
              onFinish={handleSearch}
              onFinishFailed={() => console.log('finish failure')}
              size="large"
            >
              <Row center="md">
                <Col xs={10}>
                  <Form.Item name="q">
                    <Input
                      onBlur={(e) =>
                        handleSearch({ q: e.target.value })
                      }
                      placeholder="Search for a country"
                      autoComplete="off"
                    />
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
        {!loading ? (
          <Row center="xs">
            {countries.length > 0 ? (
              <>
                {countries.map((country, index) => {
                  const {
                    _id,
                    name,
                    flag,
                    topLevelDomains,
                  } = country;

                  const exclude = [
                    '_id',
                    'name',
                    'flag',
                    'topLevelDomains',
                    '__typename',
                  ];

                  const data = handleCountryData(country, exclude);

                  return (
                    <Col xs={12} md={3} key={index}>
                      <Card
                        id={_id}
                        title={name}
                        image={flag.svgFile}
                        alt="Country Flag"
                        data={data}
                        handleAction={() => handleRedirect(_id)}
                      ></Card>
                    </Col>
                  );
                })}
              </>
            ) : (
              <Placeholder404 />
            )}
          </Row>
        ) : (
          <HomeSkeleton />
        )}
      </Grid>
    </>
  );
};
