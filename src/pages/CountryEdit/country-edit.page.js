import React, { useState, useEffect } from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import {
  StyledImage,
  StyledTitle,
  List,
  ListItem,
  StyledFormItem,
} from './country-edit.styled';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getCountry, editCountry } from '../../store/country';

import { Header } from '../../components';

import { Form, Input, Button } from 'antd';

export const CountryEdit = ({ match }) => {
  const [formData, setFormData] = useState(null);
  const history = useHistory('/');
  const dispatch = useDispatch();
  const {
    params: { id },
  } = match;

  useEffect(() => {
    dispatch(getCountry(id));
  }, []);

  const handleGoBack = (id) => {
    history.push(`/detail/${id}`);
  };

  const { selected: country, loading, error } = useSelector(
    ({ country: { selected, loading, error } }) => {
      // console.log(selected, loading, error);

      return {
        selected,
        loading,
        error,
      };
    },
  );

  useEffect(() => {
    if (country) {
      setFormData(country);
    }
  }, [country]);

  if (loading) return <p>Loading..</p>;
  if (error) return <p> Error </p>;

  const onFinish = async (data) => {
    const updated = { ...formData, ...data };
    dispatch(editCountry(updated));
    history.push(`/detail/${formData._id}`);
  };

  const exclude = [
    '_id',
    'name',
    'flag',
    'topLevelDomains',
    '__typename',
  ];

  const countryInfo = country
    ? Object.keys(country).reduce((acc, current) => {
        if (!exclude.includes(current)) {
          return [...acc, { key: current, value: country[current] }];
        } else {
          return [...acc];
        }
      }, [])
    : null;

  return (
    <>
      <Header />
      <Grid>
        {formData ? (
          <Form
            name="basic"
            initialValues={formData}
            onFinish={onFinish}
            onFinishFailed={() => console.log('finish failure')}
          >
            <Row center="md">
              <Col xs={12} md={4}>
                {country.flag && (
                  <StyledImage
                    src={country.flag.svgFile}
                    alt={country.name}
                  />
                )}
                <StyledTitle>{country.name}</StyledTitle>
              </Col>
              <Col xs={12} md={8}>
                <StyledFormItem name="_id">
                  <Input type="hidden" value={formData._id} />
                </StyledFormItem>
                <List>
                  {countryInfo.map((info, index) => {
                    const { key, value } = info;
                    return (
                      <ListItem key={index}>
                        <strong>{key}</strong>
                        <StyledFormItem name={key}>
                          <Input />
                        </StyledFormItem>
                      </ListItem>
                    );
                  })}
                  <ListItem>
                    <strong>Top level domains</strong>
                    {country.topLevelDomains.map((domain, i) => (
                      <span key={i}> {domain.name} </span>
                    ))}
                  </ListItem>
                </List>
              </Col>
            </Row>
            <Row end="md">
              <Col md={2}>
                <Button
                  onClick={() => handleGoBack(formData._id)}
                  danger
                >
                  Cancel
                </Button>
              </Col>
              <Col end="md">
                <Button htmlType="submit">Save</Button>
              </Col>
            </Row>
          </Form>
        ) : (
          <p>
            Nada a exibir, parece que esse país não está cadastrado
          </p>
        )}
      </Grid>
    </>
  );
};
