import React, { useEffect } from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import {
  StyledImage,
  StyledTitle,
  List,
  ListItem,
} from './country-detail.styled';
import { useHistory } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import { getCountry } from '../../store/country';
import { Button } from 'antd';
import { EditOutlined } from '@ant-design/icons';

import { Header } from '../../components';

export const CountryDetail = ({ match }) => {
  const {
    params: { id },
  } = match;

  const history = useHistory('/');
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCountry(id));
  }, []);

  const handleRedirect = () => {
    history.push('/');
  };

  const handleEdit = (id) => {
    history.push(`/edit/${id}`);
  };

  const { selected: country, loading, error } = useSelector(
    ({ country: { selected, loading, error } }) => {
      // console.log(selected, loading, error);
      return {
        selected,
        loading,
        error,
      };
    },
  );

  if (loading) return <p>Loading..</p>;
  if (error) return <p> Error </p>;

  // const country = data.Country.length > 0 ? data.Country[0] : null;

  const exclude = [
    '_id',
    'name',
    'flag',
    'topLevelDomains',
    '__typename',
  ];

  const countryInfo = country
    ? Object.keys(country).reduce((acc, current) => {
        if (!exclude.includes(current)) {
          return [...acc, { key: current, value: country[current] }];
        } else {
          return [...acc];
        }
      }, [])
    : null;

  return (
    <>
      <Header handleRedirect={() => handleRedirect()} />
      <Grid>
        {country ? (
          <Row center="md">
            <Col xs={12} md={4}>
              {country.flag && (
                <StyledImage
                  src={country.flag.svgFile}
                  alt="Argelia"
                />
              )}
              <StyledTitle>{country.name}</StyledTitle>
            </Col>
            <Col xs={12} md={8}>
              <List>
                {countryInfo.map((info, index) => {
                  const { key, value } = info;

                  return (
                    <ListItem key={index}>
                      <strong>{key}</strong>
                      <span>{value}</span>
                    </ListItem>
                  );
                })}
                <ListItem>
                  <strong>Top level domains</strong>
                  {country.topLevelDomains.map((domain, i) => (
                    <span key={i}> {domain.name} </span>
                  ))}
                </ListItem>
              </List>
            </Col>
          </Row>
        ) : (
          <p>
            Nada a exibir, parece que esse país não está cadastrado
          </p>
        )}
        <Row end="md">
          <Col md={6}>
            <Button
              onClick={() => handleEdit(id)}
              icon={<EditOutlined />}
            >
              Edit
            </Button>
          </Col>
        </Row>
      </Grid>
    </>
  );
};
