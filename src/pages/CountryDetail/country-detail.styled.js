import styled from 'styled-components';
import { H2 } from '../../components';

export const StyledImage = styled.img`
  width: 100%;
  box-shadow: 1px 2px 2px #f3f3f3;
  border: 1px solid #f3f3f3;
`;

export const StyledTitle = styled(H2)`
  font-size: 1.4em;
  margin-top: 12px;
  font-weight: bold;
`;

export const List = styled.ul`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  padding: 12px;
`;

export const ListItem = styled.li`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 4px;
  font-size: 1.2em;
  height: 45px;
  &:nth-child(even) {
    background: #f3f3f3;
  }

  strong {
    text-transform: capitalize;
  }
`;
