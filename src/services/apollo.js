import ApolloClient from 'apollo-boost';
import { InMemoryCache } from 'apollo-cache-inmemory';

const cache = new InMemoryCache({
  addTypename: true,
});

const client = new ApolloClient({
  cache,
  uri: 'https://countries-274616.ew.r.appspot.com',
});

export default client;
