export const checkLocalData = () => {
  return !!localStorage.getItem('gCountry');
};

export const saveData = (data) => {
  localStorage.setItem('gCountry', JSON.stringify(data));
};

export const getLocalData = () => {
  if (checkLocalData()) {
    return JSON.parse(localStorage.getItem('gCountry'));
  }
};
