import React from 'react';
import {
  StyledWrapper,
  StyledTitle,
  StyledButton,
} from './header.styled';
import { ArrowLeftOutlined } from '@ant-design/icons';

export const Header = ({ handleRedirect }) => {
  return (
    <StyledWrapper data-testid="header-component">
      {handleRedirect && (
        <StyledButton
          onClick={handleRedirect}
          icon={<ArrowLeftOutlined />}
        />
      )}
      <StyledTitle> Graph Countries app</StyledTitle>
    </StyledWrapper>
  );
};
