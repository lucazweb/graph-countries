import styled from 'styled-components';
import { H1 } from '../Typography/typography.component';
import { Button } from 'antd';

export const StyledWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 100px;
  border-bottom: 1px solid #f3f3f3;
  margin-bottom: 32px;
`;

export const StyledTitle = styled(H1)`
  font-size: 1.8em;
  font-weight: bold;
  text-transform: uppercase;
  text-align: center;
`;

export const StyledButton = styled(Button)`
  position: absolute;
  top: 12px;
  left: 12px;
`;
