import styled from 'styled-components';
import { H2 } from '../Typography/typography.component';

export const CardWrapper = styled.div.attrs({
  ['data-testid']: 'card-component',
})`
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  width: 100%;
  min-height: 250px;
  margin-bottom: 18px;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 3px;
  box-shadow: 1px 2px 2px #f3f3f3;
`;

export const StyledImage = styled.img`
  width: 100%;
  height: 170px;
  margin: auto auto 12px auto;
  border: 1px solid #f3f3f3;
  cursor: pointer;
  &:hover {
    opacity: 0.8;
  }
`;

export const CardBody = styled.div`
  display: flex;
  flex-direction: column;
`;

export const CartTitle = styled(H2).attrs({
  ['data-testid']: 'card-title',
})`
  margin-bottom: 0px;
`;

export const CardList = styled.ul`
  margin-top: 6px;
  strong {
    font-weight: bold;
  }
`;

export const CardListItem = styled.li`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 0.5em;
  padding: 3px;
  font-size: 0.8em;
  strong {
    text-transform: capitalize;
  }
  &:nth-child(even) {
    background: #f3f3f3;
  }
`;

export const CardFooter = styled.div`
  display: flex;
  justify-content: space-between;
  border-top: 1px solid #f3f3f3;
  padding-top: 8px;
  font-size: 0.8em;
  strong {
    font-weight: bold;
    padding-bottom: 8px;
  }
  span {
    font-style: italic;
  }
`;
