import React from 'react';
import { CardWrapper } from './card.styled';
import { Skeleton } from 'antd';

export const CardSkeleton = () => {
  return (
    <CardWrapper>
      <Skeleton />
    </CardWrapper>
  );
};
