import React from 'react';
import { render, findByTestId } from '@testing-library/react';
import { Card } from './card.component';

// component should render with no errors

const props = {
  name: 'Brazil',
  area: 8515767,
  capital: 'Brasília',
  population: 206135893,
  populationDensity: 24.20638011819722,
  flag: {
    svgFile: 'https://restcountries.eu/data/bra.svg',
  },
  topLevelDomains: [
    {
      name: '.br',
    },
  ],
};

describe('render tests', () => {
  test('renders with no erros', async () => {
    const { container } = render(<Card {...props} />);
    const cardNode = await findByTestId(container, 'card-component');
    const cardTitle = await findByTestId(container, 'card-title');
    const image = container.querySelector('img');

    expect(cardNode).toBeTruthy();
    expect(cardTitle).toBeTruthy();
    expect(image).toBeTruthy();
  });
});

// component should render props correctly
