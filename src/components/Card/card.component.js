import React from 'react';
import {
  CardWrapper,
  CartTitle,
  CardList,
  CardListItem,
  StyledImage,
} from './card.styled.js';

export const Card = (props) => {
  const { title, image, alt, data, handleAction } = props;

  const handleClick = () => {
    handleAction();
  };

  return (
    <CardWrapper>
      {image && (
        <StyledImage onClick={handleClick} src={image} alt={alt} />
      )}
      {title && <CartTitle>{title}</CartTitle>}
      <CardList>
        {data &&
          data.map((item, index) => (
            <CardListItem key={index}>
              <strong>{item.key}</strong>
              <span>{item.value}</span>
            </CardListItem>
          ))}
      </CardList>
    </CardWrapper>
  );
};
