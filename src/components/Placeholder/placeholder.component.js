import React from 'react';
import { StyledImg, StyledErroImg, Text } from './placeholder.styled';
import { Row, Col } from 'react-flexbox-grid';

export const Placeholder404 = () => {
  return (
    <Row center="xs">
      <>
        <Col xs={12}>
          <StyledImg />
        </Col>
        <Col xs={12}>
          <Text>Oups.. a busca não retornou resultados</Text>
        </Col>
      </>
    </Row>
  );
};

export const PlaceholderError = () => {
  return (
    <Row center="xs">
      <>
        <Col xs={12}>
          <StyledErroImg />
        </Col>
        <Col xs={12}>
          <Text>Oups.. algo deu errado.</Text>
        </Col>
      </>
    </Row>
  );
};
