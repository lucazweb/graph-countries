import styled from 'styled-components';
import { Breakpoint } from '../../config/constants';
import placeholderImg from '../../assets/countries_404.svg';
import placeholderImgError from '../../assets/countries_error.svg';

export const StyledImg = styled.div`
  width: 300px;
  height: 350px;
  background-image: url(${placeholderImg});
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
  margin: auto;
  @media (max-width: ${Breakpoint.sm}em) {
    width: 250px;
    height: 250px;
  }
`;

export const StyledErroImg = styled(StyledImg)`
  background-image: url(${placeholderImgError});
`;

export const Text = styled.h2`
  color: #333;
  font-size: 1.3em;
  margin-bottom: 18px;
  @media (max-width: ${Breakpoint.sm}em) {
    width: width 100%;
    font-size: 1.2em;
    text-align: center;
    margin-bottom: 10px;
  }
`;

export const SmallText = styled.span`
  color: #dedede;
  font-size: 1.5em;
  margin-bottom: 12px;
  @media (max-width: ${Breakpoint.sm}em) {
    font-size: 1em;
  }
`;
