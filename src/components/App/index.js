import React from 'react';
import Router from '../../Router';
import './global.scss';
import 'antd/dist/antd.css';

const App = () => <Router />;

export default App;
