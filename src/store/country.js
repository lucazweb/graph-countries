import client from '../services/apollo';
import {
  GET_COUNTRIES,
  GET_SINGLE_COUNTRY,
  SEARCH_COUNTRY,
} from '../graphql/country';
import {
  checkLocalData,
  saveData,
  getLocalData,
} from '../services/localData';

const GET_COUNTRIES_REQUEST = 'GET_COUNTRIES_REQUEST';
const GET_COUNTRIES_SUCCESS = 'GET_COUNTRIES_SUCCESS';
const GET_COUNTRIES_FAILURE = 'GET_COUNTRIES_SUCCESS';

const GET_COUNTRY_REQUEST = 'GET_COUNTRY_REQUEST';
const GET_COUNTRY_SUCCESS = 'GET_COUNTRY_SUCCESS';
const GET_COUNTRY_FAILURE = 'GET_COUNTRY_SUCCESS';

const SEARCH_COUNTRY_REQUEST = 'SEARCH_COUNTRY_REQUEST';
const SEARCH_COUNTRY_SUCCESS = 'SEARCH_COUNTRY_SUCCESS';
const SEARCH_COUNTRY_FAILURE = 'SEARCH_COUNTRY_SUCCESS';

const EDIT_COUNTRY_REQUEST = 'EDIT_COUNTRY';
const EDIT_COUNTRY_SUCCESS = 'EDIT_COUNTRY_SUCCESS';
const EDIT_COUNTRY_FAILURE = 'EDIT_COUNTRY_SUCCESS';

export const getCountries = () => async (dispatch) => {
  dispatch({
    type: GET_COUNTRIES_REQUEST,
  });

  try {
    const {
      data: { Country },
    } = await client.query({
      query: GET_COUNTRIES,
    });

    if (!checkLocalData()) {
      localStorage.setItem('gCountry', JSON.stringify(Country));
    }

    dispatch({
      type: GET_COUNTRIES_SUCCESS,
      payload: Country,
    });
  } catch (err) {
    console.log(err);
  }
};

export const getCountry = (id) => async (dispatch) => {
  dispatch({
    type: GET_COUNTRY_REQUEST,
  });

  try {
    if (checkLocalData()) {
      const countries = getLocalData();
      const [country] = countries.filter((ctr) => ctr._id === id);

      dispatch({
        type: GET_COUNTRY_SUCCESS,
        payload: {
          data: country,
        },
      });
    } else {
      const {
        data: { Country },
      } = await client.query({
        query: GET_SINGLE_COUNTRY,
        variables: {
          id: String(id),
        },
      });

      const [selected] = Country;

      dispatch({
        type: GET_COUNTRY_SUCCESS,
        payload: {
          data: selected,
        },
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({
      type: GET_COUNTRIES_FAILURE,
    });
  }
};

export const editCountry = (country) => (dispatch) => {
  //   console.log('from edit country', country);
  dispatch({
    type: EDIT_COUNTRY_REQUEST,
  });

  try {
    // get data from cache or remote and store in cache

    if (localStorage.getItem('gCountry')) {
      const countries = JSON.parse(
        localStorage.getItem('gCountry'),
      ).filter((ctr) => ctr._id !== country._id);

      const updated = [...countries, country];

      saveData(updated);
      dispatch({
        type: EDIT_COUNTRY_SUCCESS,
        selected: country,
      });
    }
  } catch (err) {
    console.log(err);
  }
};

export const searchCountry = (q) => async (dispatch) => {
  dispatch({
    type: SEARCH_COUNTRY_REQUEST,
  });

  try {
    const {
      data: { Country },
    } = await client.query({
      query: SEARCH_COUNTRY,
      variables: {
        name: q,
      },
    });

    dispatch({
      type: SEARCH_COUNTRY_SUCCESS,
      payload: Country,
    });
  } catch (err) {
    console.log(err);
  }
};

const initialState = {
  countries: [],
  selected: null,
  error: null,
  loading: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_COUNTRIES_REQUEST:
    case GET_COUNTRY_REQUEST:
      return { ...state, loading: true, error: null };

    case GET_COUNTRIES_SUCCESS:
    case SEARCH_COUNTRY_SUCCESS:
      return {
        ...state,
        countries: action.payload,
        loading: false,
        error: null,
      };

    case GET_COUNTRY_SUCCESS:
      const country = action.payload.data;
      return {
        ...state,
        selected: country,
        loading: false,
        error: null,
      };

    case GET_COUNTRIES_FAILURE:
    case GET_COUNTRY_FAILURE:
    case SEARCH_COUNTRY_FAILURE:
    case EDIT_COUNTRY_FAILURE:
      return { ...state, error: 'Algo deu errado', loading: false };

    default:
      return state;
  }
}
