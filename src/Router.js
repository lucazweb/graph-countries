import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { Home, CountryDetail, CountryEdit } from './pages';

export const Router = () => (
  <BrowserRouter>
    <Route exact path="/" component={Home} />
    <Route path="/detail/:id" component={CountryDetail} />
    <Route path="/edit/:id" component={CountryEdit} />
  </BrowserRouter>
);

export default Router;
