import { gql } from 'apollo-boost';

export const GET_COUNTRIES = gql`
  query {
    Country {
      _id
      name
      area
      capital
      population
      populationDensity
      capital
      flag {
        svgFile
      }
      topLevelDomains {
        name
      }
    }
  }
`;

export const GET_SINGLE_COUNTRY = gql`
  query($id: String!) {
    Country(_id: $id) {
      _id
      name
      area
      capital
      population
      populationDensity
      capital
      flag {
        svgFile
      }
      topLevelDomains {
        name
      }
    }
  }
`;

export const SEARCH_COUNTRY = gql`
  query($name: String!) {
    Country(filter: { name_contains: $name }) {
      _id
      name
      capital
      flag {
        svgFile
      }
    }
  }
`;

export const EDIT_COUNTRY = gql`
  mutation($id: String!, $population: String!) {
    editCountry(_id: $id, population: $population) {
      success
      Country {
        _id
        name
        population
      }
    }
  }
`;

export const UPDATE_COUNTRY = gql`
  mutation UpdateCountry(
    $_id: String!
    $area: Float
    $population: Float
    $populationDensity: Float
    $capital: String
  ) {
    updateCountry(
      _id: $_id
      area: $area
      population: $population
      populationDensity: $populationDensity
      capital: $capital
    ) @client
  }
`;
