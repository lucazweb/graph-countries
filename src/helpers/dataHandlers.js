export const handleCountryData = (country, exclude) => {
  return Object.keys(country).reduce((acc, current) => {
    if (!exclude.includes(current)) {
      return [...acc, { key: current, value: country[current] }];
    } else {
      return [...acc];
    }
  }, []);
};
